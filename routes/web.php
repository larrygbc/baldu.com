<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['invitado']], function(){
	Route::get('/', 'HomeController@getHome');
	Route::get('/iniciarsesion/{mensaje?}', 'HomeController@getIniciarSesion');
	Route::get('/registrarusuario', 'HomeController@getRegistrar');
	Route::get('/recuperar-contraseña/{mensaje?}', 'HomeController@RecuperarContraseña');
	Route::post('/form-recuperar-contraseña', 'ValidarController@Contraseña');
	Route::get('/producto', 'HomeController@getbusquedaIndex');
});

Route::prefix('auth')->group(function(){
	Route::middleware(['user'])->group(function(){
		Route::get('/{usuario?}', 'HomeController@getLogin');
		Route::get('/{usuario?}/producto', 'HomeController@getbusqueda');
		Route::get('/{usuario?}/producto/confirmar-compra', 'HomeController@getConfirmarCompra');
		//perfil del usuario
		Route::get('/{usuario?}/perfil/', 'HomeController@Perfil');
		//baja de usuario
		Route::post('/{usuario?}/perfil/baja', 'HomeController@BorrarCuenta');
		//modificar perfil
		Route::post('/{usuario?}/modificar-perfil/', 'HomeController@ModificarPerfil');
		Route::get('/{usuario?}/compras/', 'HomeController@Compras');
		Route::get('/{usuario?}/compras/detalles/{compra}', 'HomeController@DetalleCompra');
	});
});

Route::post('/validarusuario', 'ValidarController@postValidarUsuario');
Route::post('/resgistrado=ok', 'ValidarController@postRegistrar');
Route::get('/cerrarsesion', 'HomeController@getLogout');

// Consultas Ajax usuario
//añadir productos al carrito
Route::post('/carrito', 'HomeController@postCarrito');
//enseñar productos guardados
Route::post('/carritoguardado', 'HomeController@postCarritoGuardado');
//Eliminar producto
Route::post('/CarritoDelete', 'HomeController@postCarritoEliminar');
//confirmar compra
Route::post('/confirmar-compra', 'HomeController@ValidarCompra');
Route::post('/filtro', 'HomeController@postfiltro');
Route::post('registrar/comprobarusuario/{id?}', 'ValidarController@postComprobarUser');
Route::post('registrar/comprobaremail/{id?}', 'ValidarController@postComprobarEmail');


//Rutas Tienda
Route::prefix('admin')->group(function(){
	Route::get('/iniciar-sesion/{mensaje?}', 'TiendaController@IniciarSesion');
	Route::get('/restaurar-contraseña/{mensaje?}', 'TiendaController@RecuperarContraseña');
	Route::post('/form-recuperar-contraseña', 'ValidarController@ContraseñaTienda');
	Route::get('/login/{id}', 'TiendaController@Login');
	Route::get('/login/{id}/perfil', 'TiendaController@Perfil');
	Route::get('/login/{id}/modificar-perfil', 'TiendaController@ModificarPerfil');
	Route::post('/login/{id}/modificar-admin', 'TiendaController@ModificarAdmin');
	//añadir un producto
	Route::get('/login/{id}/añadir-producto/', 'TiendaController@AñadirProducto');
	//confirmacion al guardar un producto
	Route::post('/login/{id}/guardar-producto/', 'TiendaController@GuardarProducto');
	
	Route::get('/login/{id}/modificar-producto/', 'TiendaController@ModificarProducto');
	Route::get('/login/{id}/modificar-producto/{id_producto}', 'TiendaController@Producto');
	//confirmar modificacion de un producto
	Route::post('/login/{id}/modificar-producto/{id_producto}/ok', 'TiendaController@ProductoOk');
	//borrar producto
	Route::get('/login/{id}/eliminar-producto', 'TiendaController@EliminarProducto');
	Route::get('/login/{id}/eliminar/{id_producto}', 'TiendaController@Eliminar');
	//cerrar sesion
	Route::get('/login/{id}/cerrar-sesion', 'TiendaController@CerrarSesion');
	//darse de baja
	Route::post('/login/{id}/baja', 'TiendaController@BorrarCuenta');
});

Route::post('/ValidarTienda', 'ValidarController@ValidarTienda');
Route::get('/registrartienda', 'TiendaController@RegistrarTienda');

// Ajax tienda
Route::post('/ValidarTiendaEmail', 'TiendaController@ValidarTiendaEmail');
Route::post('/resgistrotienda=ok', 'ValidarController@RegistroTienda');

//para el registro de la tienda
Route::post('/TiendaCodigoPostal', 'TiendaController@RegistroCodigoPostal');
Route::post('/TiendaBarrio', 'TiendaController@RegistroBarrio');











