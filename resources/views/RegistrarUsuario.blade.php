
@extends('layouts/bootstrap')

@section('title')
Baldu
@endsection

@section('content')
<div class="container ">
	<div class="row" style="height: 100vh;">
		<div class="col-12 text-center d-flex justify-content-center align-items-center">
			<div class="box-in-ss mx-auto" style="width: 600px;">
				<h3 class="text-left">Baldu</h3>
				<p class="text-left mb-4">Bienvenido a nuestra pagina de registro</p>
				
				<form action="/registrado=ok" method="post" id="Reg-form">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="text">Usuario</label>
						<input type="text" class="form-control" id="Reg-user"  name="Reg-user" >
						<div class="error" style="display: none;" id="Error-user"></div>
					</div>
					<div class="form-group">
						<label for="text">Email</label>
						<input type="email" class="form-control" id="Reg-email"  name="email" >
						<div class="error" style="display: none;" id="Error-email"></div>
					</div>
					<div class="form-group">
						<label for="Password">Contraseña</label>
						<input type="password" class="form-control" id="Reg-password"  name="Reg-password" >
						<div class="error" style="display: none;" id="Error-pass"></div>
					</div>
					<div class="form-group">
						<label for="text">Nombre</label>
						<input type="text" class="form-control" id="Reg-nombre"  name="Regnombre" >
						<div class="error" style="display: none;" id="Error-nombre"></div>
					</div>
					<div class="form-group">
						<label for="text">Apellidos</label>
						<input type="text" class="form-control" id="Reg-apellidos"  name="Reg-apellidos" >
						<div class="error" style="display: none;" id="Error-cognom"></div>
					</div>
					<div class="form-group">
						<label for="text">Telefono</label>
						<input type="tel" class="form-control" id="Reg-tel"  name="Reg-tel" >
						<div class="error" style="display: none;" id="Error-tel"></div>
					</div>
					<div class="form-group">
						<label for="text" class="">Fecha de nacimiento</label>
						<input type="date" class="form-control" id="Reg-date"  name="Reg-date" >
						<div class="error" style="display: none;" id="Error-date"></div>
					</div>
					<div class="form-group">
						<label for="text">Codigo postal</label>
						<input type="text" class="form-control" id="Reg-cod"  name="Reg-cod" >
						<div class="error" style="display: none;" id="Error-cod"></div>
					</div>
					<div class="form-group">
						<label for="text">Direccion</label>
						<input type="text" class="form-control" id="Reg-direccion"  name="Reg-direccion" >
						<div class="error" style="display: none;" id="Error-direccion"></div>
					</div>
					<div class="form-group  mt-4 float-right">
						<input type="submit" class="btn btn-raised btn-in-ss" value="Aceptar" id="btn-reg-conf">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('script')
<script src="http://baldu.com/public/js/Form_registro.js"></script>
@endsection

