
@extends('layouts/bootstrap')

@section('title')
Baldu
@endsection

@section('content')

<div class="container ">
	<div class="row" style="height: 100vh;">
		<div class="col-12 text-center d-flex justify-content-center align-items-center">
			<div class="box-in-ss mx-auto" style="width: 400px;">
				<h3 class="text-left">Baldu tienda</h3>
				<p class="text-left mb-4">Inicia sesion con la cuenta de tu tienda</p>
				<i class="material-icons logo-in-ss">store</i>
				<form action="{{ action('ValidarController@ValidarTienda') }}" method="post" id="form-ini-tnd">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="email" class="bmd-label-floating">Email</label>
						<input type="email" class="form-control" id="email" style="display: unset;" name="email">
						<div class="error" id="Er-ini-email" style="display: none;"></div>
					</div>
					<div class="form-group">
						<label for="Password" class="bmd-label-floating">Contraseña</label>
						<input type="password" class="form-control" id="password" style="display: unset;" name="password">
						<div class="error" id="Er-ini-pass" style="display: none;"></div>
					</div>
					<div class="form-group text-left pt-2">
						<a href="{{ action('TiendaController@RecuperarContraseña') }}">Recuperar Contraseña</a>
					</div>
					<div class="form-group d-flex justify-content-between align-items-center">
						<a href="{{ action('TiendaController@RegistrarTienda') }}" class="pt-1">Registrar tienda</a>
						<input type="submit" class="btn btn-raised btn-in-ss" value="Aceptar">
					</div>
				</form>
				<div class="error text-center">
					<p>{{ $mensaje }}</p>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('script')
<script src="http://baldu.com/public/js/index.js"></script>
<script src="http://baldu.com/public/js/jquery-asRange.min.js"></script>
@endsection