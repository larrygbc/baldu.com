@extends('layouts/LoginTienda')

@section('title')
Administrador Baldu
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="section mt-3" style="justify-content: center;">
      <i class="material-icons" style="font-size: 54px">shopping_basket</i>
      <h1 class="text-center pl-3">Añadir productos</h1>
    </div>
    <div class="section-card mt-5">
      <form action="{{ action('TiendaController@GuardarProducto', $id) }}" class="p-3" method="post" enctype="multipart/form-data" id="Form-add">
        {{ csrf_field() }}

        <div class="form-group">
          <label for="exampleFormControlFile1">Foto del producto</label>
          <input type="file" class="form-control-file" name="imagen" id="add-imagen">
          <div class="error" class="dnone" id="Er-add-imagen"></div>
        </div>
        <div class="form-group">
          <label for="">Nombre</label>
          <input type="text" class="form-control" id="add-nombre" name="nombre">
          <div class="error" class="dnone" id="Er-add-nombre"></div>
        </div>
        <div class="form-group">
          <label for="">Stock</label>
          <input type="text" class="form-control" id="add-stock" name="stock">
          <div class="error" class="dnone" id="Er-add-stock"></div>
        </div>
        <div class="form-group">
          <label for="">Precio</label>
          <input type="text" class="form-control" id="add-precio" name="precio">
          <div class="error" class="dnone" id="Er-add-precio"></div>
        </div>
        <div class="form-group">
          <label for="exampleTextarea">Descripcion</label>
          <textarea class="form-control" id="add-desc" rows="3" name="descripcion"></textarea>
        </div>
        <div class="form-group">
          <label for="">Categoria</label>
          <select name="categoria" class="form-control" id="add-cat">
            @foreach($categoria as $value)
            <option id="{{ $value->id }}">{{ $value->categoria }}</option>
            @endforeach
          </select>
        </div>
        <div>
          <input type="submit" class="btn btn-raised btn-in-ss" value="Añadir"/>
        </div>
        
      </form>
    </div>
  </div>
</div>

@endsection

@section('script')
<script src="http://baldu.com/public/js/admin.js"></script>
@endsection