@extends('layouts/LoginTienda')

@section('title')
Administrador Baldu
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="section mt-3" style="justify-content: center;">
      <i class="material-icons" style="font-size: 54px">delete</i>
      <h1 class="text-center pl-3">Eliminar productos</h1>
    </div>
    <div class="section-card mt-5 p-3">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">id producto</th>
            <th scope="col">Nombre</th>
            <th scope="col">Stock</th>
            <th scope="col">Precio</th>
          </tr>
        </thead>
        <tbody>
         @foreach($productos as $value)
         <tr>
          <th scope="row">{{ $value->id }}</th>
          <td>{{ $value->nombre }}</td>
          <td>{{ $value->stock }}</td>
          <td>{{ $value->precio}}</td>
          <td><a href="{{ action('TiendaController@Eliminar',[ 
          'id' =>$id, 
          'id_producto' =>$value->id
          ]) }}"><button class="btn btn-raised btn-in-ss">Eliminar</button></a></td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>

@endsection

@section('script')
<script src="http://baldu.com/public/js/admin.js"></script>
@endsection