@extends('layouts/LoginTienda')

@section('title')
Administrador Baldu
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="section mt-3" style="justify-content: center;">
      <i class="material-icons" style="font-size: 54px">store</i>
      <h1 class="text-center pt-4">Perfil</h1>
    </div>
    <div class="section-card mt-4">
      <div class="p-3">
        <div class="d-inline">
          <h6>Id:</h6>
          <p>{{ $id }}</p>
        </div>
        <div class="d-inline">
          <h6>CIF:</h6>
          <p>{{ $cif }}</p>
        </div>
        <div class="d-inline">
          <h6>Nombre:</h6>
          <p>{{ $nombre }}</p>
        </div>
        <div class="d-inline">
          <h6>Email:</h6>
          <p>{{ $email }}</p>
        </div>
        <div class="d-inline">
          <h6>Direccion:</h6>
          <p>{{ $direccion }}</p>
        </div>
        <div class="d-inline">
          <h6>Codigo postal:</h6>
          <p>{{ $codigo_postal }}</p>
        </div>
        <div class="d-inline">
          <h6>Barrio:</h6>
          <p>{{ $barrio }}</p>
        </div>
        <div class="d-inline">
          <h6>Telefono:</h6>
          <p>{{ $telefono }}</p>
        </div>
      </div>
    </div>
    <div class="col-12 mt-3">
      <a href="{{ action('TiendaController@ModificarPerfil', $id) }}">
        <button class="btn btn-raised btn-sm btn-in-ss">Modificar informacion</button>
      </a>
    </div>
  </div>
  <div class="col-12 mt-3">
    <div class="alert alert-danger" role="alert">
      <h4>Darse de baja de baldu</h4>
      <p>Al darse de baja se borrara toda la informacion relacionada con tu tienda</p>
      <button type="button" class="btn btn-raised btn-in-ss" data-toggle="modal" data-target="#exampleModalCenter">Aceptar</button>
    </div>
  </div>
</div>

{{-- modal para dar de baja --}}
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Darse de baja</h5>
      </div>
      <form action="{{ action('TiendaController@BorrarCuenta', $id) }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" value="{{ $id }}">
        <div class="modal-body">
          <i class="material-icons d-flex justify-content-center" style="font-size: 110px; color: #80808094;">mood_bad</i>
          <p class="d-flex justify-content-center mt-3 text-danger" style="font-size: 16px;">Confirmar la baja de {{ $nombre }} en baldu.com</p>
        </div>
        <div class="modal-footer">
          <button type="reset" class="btn btn-secondary">Cerrar</button>
          <button type="submit" class="btn btn-primary">Aceptar</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="http://baldu.com/public/js/admin.js"></script>
@endsection