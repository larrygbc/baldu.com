
@extends('layouts/bootstrap')

@section('title')
Baldu
@endsection

@section('content')
<div class="container ">
	<div class="row" style="height: 100vh;">
		<div class="col-12 text-center d-flex justify-content-center align-items-center">
			<div class="box-in-ss mx-auto" style="width: 600px;">
				<h3 class="text-left">Baldu tienda</h3>
				<p class="text-left mb-4">Bienvenido a nuestra pagina de registro para las tiendas</p>
				
				<form action="" method="post" id="Tnd-form">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="text">Nombre</label>
						<input type="text" class="form-control" id="Tnd-nombre"  name="Tnd_name" >
						<div class="error" style="display: none;" id="Error-Tnombre"></div>
					</div>
					<div class="form-group">
						<label for="text">Email</label>
						<input type="email" class="form-control" id="Tnd-email"  name="Tnd_email" >
						<div class="error" style="display: none;" id="Error-Temail"></div>
					</div>
					<div class="form-group">
						<label for="Password">Contraseña</label>
						<input type="password" class="form-control" id="Tnd-password"  name="Tnd_password" >
						<div class="error" style="display: none;" id="Error-Tpass"></div>
					</div>
					<div class="form-group">
						<label for="text">CIF</label>
						<input type="text" class="form-control" id="Tnd-cif"  name="Tnd_cif" >
						<div class="error" style="display: none;" id="Error-Tcif"></div>
					</div>
					<div class="form-group">
						<label for="Tnd-cod" class="bmd-label-floating">Codigo postal</label>
						<select class="form-control" id="Tnd-cod" name="Tnd-cod">
							<option id='0'></option>
						</select>
						<div class="error" style="display: none;" id="Error-Tcod"></div>
					</div>
					<div class="form-group">
						<label for="text">Barrio</label>
						<select class="form-control" id="Tnd-barrio" name="Tnd_barrio">
							<option id='0'></option>
						</select>
						<div class="error" style="display: none;" id="Error-Tbarrio"></div>
					</div>
					
					<div class="form-group">
						<label for="text" class="">Direccion</label>
						<input type="text" class="form-control" id="Tnd-direccion"  name="Tnd_direccion" >
						<div class="error" style="display: none;" id="Error-Tdireccion"></div>
					</div>
					<div class="form-group">
						<label for="text">Telefono</label>
						<input type="tel" class="form-control" id="Tnd-tel"  name="Tnd_tel" >
						<div class="error" style="display: none;" id="Error-Ttel"></div>
					</div>
					<div class="form-group  mt-4 float-right">
						<input type="submit" class="btn btn-raised btn-in-ss" value="Aceptar" id="btn-Tnd-conf">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection


@section('script')
<script src="http://baldu.com/public/js/Form_RegTienda.js"></script>
@endsection

