@extends('layouts/LoginTienda')

@section('title')
Administrador Baldu
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="section mt-3" style="justify-content: center;">
      <i class="material-icons" style="font-size: 54px">create</i>
      <h1 class="text-center pl-3">Modificar Perfil</h1>
    </div>
    <div class="section-card mt-4">
      <form  method="post" action="{{ action('TiendaController@ModificarAdmin', $id) }}" class="p-4" id="Tnd-Mod-form">
        {{ csrf_field() }}

        <div class="form-group">
          <label for="" class="bmd-label-floating">Nombre</label>
          <input type="text" class="form-control" id="Tnd-Mod-n" name="Tnd_Mod_nombre" value="{{ $nombre }}">
          <div class="error" id="Tnd-error-n"></div>
        </div>
        <div class="form-group">
          <label for="" class="bmd-label-floating">Cif</label>
          <input type="text" class="form-control" id="Tnd-Mod-c" name="Tnd_Mod_cif" value="{{ $cif }}">
          <div class="error" id="Tnd-error-c"></div>
        </div>
        
        <div class="form-group">
          <label for="" class="bmd-label-floating">Direccion</label>
          <input type="text" class="form-control" id="Tnd-Mod-d" name="Tnd_Mod_direccion" value="{{ $direccion }}">
          <div class="error" id="Tnd-error-d"></div>
        </div>
        
        <div class="form-group">
          <label for="" class="bmd-label-floating">Codigo postal</label>
          <select class="form-control" id="Tnd-Mod-cd" name="Tnd_Mod_cod">
            <option id="p-fixed">{{ $sel_cod }}</option>
            @foreach($codigo_postal as $value)
            <option id="{{ $value->id }}">{{ $value->codigo_postal }}</option>
            @endforeach
          </select>
        </div>
        
        <div class="form-group">
          <label for="" class="bmd-label-floating">Barrio</label>
          <select class="form-control" id="Tnd-Mod-b" name="Tnd_Mod_barrio">
            <option id="b-fixed">{{ $sel_barrio }}</option>
            @foreach($barrio as $value)
            <option id="{{ $value->id }}">{{ $value->nombre }}</option>
            @endforeach
          </select>
        </div>
        
        <div class="form-group">
          <label for="" class="bmd-label-floating">Telefono</label>
          <input type="text" class="form-control" id="Tnd-Mod-t" name="Tnd_Mod_tel" value="{{ $telefono }}">
          <div class="error" id="Tnd-error-t"></div>
        </div>
        <div class="form-group mt-3">
          <input class="btn btn-raised btn-sm btn-in-ss" type="submit" value="Modificar" id="Tnd-Mod-evr">
        </div>
      </form>
    </div>
  </div>
</div>

@endsection

@section('script')
<script src="http://baldu.com/public/js/admin.js"></script>
@endsection