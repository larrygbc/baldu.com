@extends('layouts/LoginTienda')

@section('title')
Administrador Baldu
@endsection

@section('content')
<h1 class="text-center pt-4">Bienvenido al apartado administrador</h1>
<div class="section-card mt-4">
  <div class="section pl-3 pt-3">
    <i class="material-icons">info</i>
    <h4 class=" pl-2 d-inline">Info</h4>
  </div>
  <div class="p-3">
    <div class="d-inline">
      <h6>Id:</h6>
      <p>{{ $id }}</p>
    </div>
    <div class="d-inline">
      <h6>CIF:</h6>
      <p>{{ $cif }}</p>
    </div>
    <div class="d-inline">
       <h6>Nombre:</h6>
       <p>{{ $nombre }}</p>
    </div>
    <div class="d-inline">
      <h6>Email:</h6>
      <p>{{ $email }}</p>
    </div>
    <div class="d-inline">
      <h6>Direccion:</h6>
      <p>{{ $direccion }}</p>
    </div>
    <div class="d-inline">
      <h6>Codigo postal:</h6>
      <p>{{ $codigo_postal }}</p>
    </div>
    <div class="d-inline">
      <h6>Barrio:</h6>
      <p>{{ $barrio }}</p>
    </div>
    <div class="d-inline">
      <h6>Telefono:</h6>
      <p>{{ $telefono }}</p>
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="http://baldu.com/public/js/admin.js"></script>
@endsection