@extends('layouts/LoginTienda')

@section('title')
Administrador Baldu
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="section mt-3" style="justify-content: center;">
      <i class="material-icons" style="font-size: 54px">create</i>
      <h1 class="text-center pl-3">Modificar producto</h1>
    </div>
    <div class="section-card mt-5 p-3">
      @foreach($producto as $value)
      <form action="{{ action('TiendaController@ProductoOk', [$id, $value->id]) }}" class="p-3" method="post" enctype="multipart/form-data" id="Form-add">
        {{ csrf_field() }}
        
        <div class="text-center image">
          <img src="http://baldu.com/public/imagen/{{ $value->ruta }}" alt="{{ $value->nombre }}" class="foto sombra-foto">
        </div>
        <div class="form-group">
          <label for="exampleFormControlFile1">Foto del producto</label>
          <input type="file" class="form-control-file" name="imagen" id="add-imagen">
          <div class="error" class="dnone" id="Er-add-imagen"></div>
        </div>
        <div class="form-group">
          <label for="">Nombre</label>
          <input type="text" class="form-control" id="add-nombre" name="nombre"  value="{{ $value->nombre }}">
          <div class="error" class="dnone" id="Er-add-nombre"></div>
        </div>
        <div class="form-group">
          <label for="">Stock</label>
          <input type="text" class="form-control" id="add-stock" name="stock" value="{{ $value->stock }}">
          <div class="error" class="dnone" none;" id="Er-add-stock"></div>
        </div>
        <div class="form-group">
          <label for="">Precio</label>
          <input type="text" class="form-control" id="add-precio" name="precio" value="{{ $value->precio }}">
          <div class="error" class="dnone" id="Er-add-precio"></div>
        </div>
        <div class="form-group">
          <label for="exampleTextarea">Descripcion</label>
          <textarea class="form-control" id="add-desc" rows="3" name="descripcion">{{ $value->descripcion }}</textarea>
        </div>
        
        @php
        $clave = $value->categoria - 1;
        @endphp

        <div class="form-group">
          <label for="">Categoria</label>
          <select name="categoria" class="form-control" id="add-cat">
            @foreach ($categoria as $key => $value)
            <option value="{{ $value->id }}"
            @if ($key == $clave)
            selected="selected"
            @endif
            >{{ $value->categoria }}</option>
            @endforeach
          </select>
        </div>
        <div>
          <input type="submit" class="btn btn-raised btn-in-ss" value="modificar"/>
        </div>

      </form>
      @endforeach
    </div>
  </div>
</div>

@endsection

@section('script')
<script src="http://baldu.com/public/js/admin.js"></script>
@endsection