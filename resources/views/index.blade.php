@extends('layouts/master')

@section('title')
Baldu
@endsection

@section('css')
<link rel="stylesheet" href="http://baldu.com/public/css/index.css">
@endsection


@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection


@section('content')

<div class="container dnone" id="filtro">
	<div class="row mt-1" id="row-filtro">
		<div class="col-12 bg-success filtro">
			<form action="{{ action('HomeController@getbusquedaIndex') }}" method="get">
				<input type="hidden" value="filtro" name="filtro">
				<div class="form-row">
					<div class="form-group col-md-4 col-12">
						<label for="">Buscador</label>
						<input class="form-control" placeholder="Buscar..." type="text" id="input-buscar" name="search" style="height: 41px;">
					</div>
					<div class="form-group col-12 col-md-4">
						<label for="" class="bmd-label-floating">Categoria</label>
						<select name="sel_cat" id="id-cat" class="form-control" name="cat">
						</select>
					</div>
					<div class="form-group col-12 col-md-4">
						<label for="">Codigo postal</label>
						<select name="sel_cod" id="cod_postal" class="form-control">
						</select>
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-12 col-md-4">
						<label for="">Barrio</label>
						<select name="sel_barrio" id="barrio" class="form-control">
						</select>
					</div>
					<div class="form-group col-md-4 col-12">
						<label for="">Min</label>
						<input type="text" id="min" class="form-control" name="sel_min" style="height: 41px;" value="0">
					</div>
					<div class="form-group col-md-4 col-12">
						<label for="">Max</label>
						<input type="text" id="max" class="form-control" name="sel_max"  style="height: 41px;" value=500>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-12 text-center d-flex align-items-center justify-content-center mt-3">
						<button id="btn-form-cat" class="btn btn-raised btn-sm btn-in-ss ">Buscar</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
{{-- Contenedor de los productos --}}
<div class="container">
	<div class="card-columns mt-4 mx-auto" id="row_card">
		@foreach($productos as $key => $value)
		<div class='tarjeta mb-3'>
			<div >
				@if(empty($value->ruta))
				<img src="" alt='' class="foto-producto" />
				@else
				<img src="http://baldu.com/public/imagen/{{ $value->ruta }}" alt='' class="foto-producto" />
				@endif
			</div>
			<div class='card-body' >
				<div class='d-flex justify-content-between align-items-end' id='card-body'>
					<h5 class='card-titulo' id=''>{{ $value->nombre }}</h5>
					<p class='card-precio' id=''>{{ $value->precio }}€</p>
				</div>
				<p class='card-subtitulo'>{{ $value->descripcion }}</p>
			</div>
			<div class='card-footer'>
				<button type='button' class='btn btn-raised btn-sm btn-in-ss d-flex justify-content-center align-items-center' id=''>
					<i class='material-icons'>add_shopping_cart</i>
				</button>
			</div>
		</div>
		@endforeach
	</div>
</div>

@endsection


@section('script')
<script src="http://baldu.com/public/js/index.js"></script>
@endsection

