<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="icon" href="../../../public/imagen/favicon.ico">

  <!-- Material Design for Bootstrap fonts and icons -->
  <link rel="stylesheet" href="http://baldu.com/public/css/fuentes.css">

  <!-- Material Design for Bootstrap CSS -->
  <link rel="stylesheet" href="http://baldu.com/public/css/bootstrap-material-design.min.css">

  <!--iconos material design-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
  rel="stylesheet">

  <link rel="stylesheet" href="http://baldu.com/public/css/my-style.css">

  <link rel="stylesheet" href="http://baldu.com/public/css/fontawesome-all.min.css">

  {{-- notificaciones snackbar --}}
  <link rel="stylesheet" href="http://baldu.com/public/css/material.css">
  <link rel="stylesheet" href="http://baldu.com/public/css/snackbar.min.css">
  


  <title>@yield('title')</title>

  @yield('css')

  @yield('meta')
  

</head>
<body>

  <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay">
    <header class="bmd-layout-header">
      <div class="navbar navbar-light bg-success d-flex justify-content-between">

        <div class="mx-auto m-3 m-md-0">
          <div class="d-flex align-items-center">
            <h4 class="tt-baldu">
              <a href="{{ action('HomeController@getHome') }}">Baldu</a>
            </h4>
          </div>
        </div>
        {{-- buscador --}}
        <div class="buscador mx-auto" id="buscador">
          <form class="d-flex justify-content-between align-items-center" action="{{ action('HomeController@getbusquedaIndex') }}" method="get" id="form">
            <button class="btn bmd-btn-icon button-buscar fl-none" type="submit" >
              <i class="material-icons text-white" id="icon-buscar">search</i>
            </button>
            <span class="bmd-form-group ">
              <input class="input-buscar flex-column color-place-white" placeholder="Buscar..." type="text" id="input-buscar" name="search">
            </span>
            <button class="btn bmd-btn-icon button-buscar fl-none" type="reset">
              <i class="material-icons text-white" id="icon-close">close</i>
            </button>
          </form>
          <button class="btn btn-raised btn-in-ss" id="btn-filtro">filtros</button>
        </div>
        
        <div class="float-md-right mx-md-0 mx-auto mt-1">
          <a href="{{ action('HomeController@getIniciarSesion') }}" class="d-flex align-items-center">
            <button type="button" class="btn btn-raised btn-sm btn-in-ss">Iniciar sesion</button>
          </a>
        </div>
        
      </div>
    </header>

    {{-- menu del perfil --}}
    <div id="dw-p1" class="bmd-layout-drawer bg-faded">
      <header>
        <a class="navbar-brand">Title</a>
      </header>
      <ul class="list-group">
        <a class="list-group-item">Link 1</a>
        <a class="list-group-item">Link 2</a>
        <a class="list-group-item">Link 3</a>
      </ul>
    </div>
    {{-- contenido de la pagina --}}
    <main class="bmd-layout-content" style="min-height: 90vh;">
      @yield('content')
    </main>
  </div>
  

  {{-- footer --}}
  <footer class="container-fluid text-white" style="background-color: #cecece;">
    <div class="row mt-1">
      <div class="col-12">

      </div>
    </div>
    <div class="row m-2">
      <div class="col-12 d-flex justify-content-center">
        <i class="fab fa-facebook-square mr-3 rs"></i>
        <i class="fab fa-twitter mr-3 rs"></i>
        <i class="fab fa-instagram mr-3 rs"></i>
      </div>
    </div>
    <div class="row p-2">
      <div class="col-12 d-flex justify-content-center">
        <span>Sitio web creado por @Larrygbc.</span>
      </div>
    </div>
  </footer>

  <!-- JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="http://baldu.com/public/js/jquery-3.3.1.min.js"></script>
  <script src="http://baldu.com/public/js/popper.js"></script>
  {{-- script material design --}}
  <script src="http://baldu.com/public/js/bootstrap-material-design.min.js"></script>
  {{-- Iniciar el script de bootstrap --}}
  <script>
    $(document).ready(function() { $('body').bootstrapMaterialDesign(); });
  </script>
  
  <script src="http://baldu.com/public/js/snackbar.js"></script>

  @yield('script')
</body>
</html>