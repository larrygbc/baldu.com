<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="icon" href="../../../public/imagen/favicon.ico">

  <!-- Material Design for Bootstrap fonts and icons -->
  <link rel="stylesheet" href="http://baldu.com/public/css/fuentes.css">

  <!-- Material Design for Bootstrap CSS -->
  <link rel="stylesheet" href="http://baldu.com/public/css/bootstrap-material-design.min.css">

  <!--iconos material design-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
  rel="stylesheet">
  
  <link rel="stylesheet" href="http://baldu.com/public/css/my-style.css">

  <link rel="stylesheet" href="http://baldu.com/public/css/fontawesome-all.min.css">

  {{-- notificaciones snackbar --}}
  <link rel="stylesheet" href="http://baldu.com/public/css/material.css">
  <link rel="stylesheet" href="http://baldu.com/public/css/snackbar.min.css">

  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <title>@yield('title')</title>
</head>
<body>
  <div class="container-fluid" >
    <div class="row">
      <div class="col-12 bg-success ct-tt" style="padding: 10px 20px">
        <h4 class="tt-baldu">Baldu</h4>
        <div>
          <a href="{{ action('TiendaController@CerrarSesion', $id) }}">
            <button class="btn btn-raised btn-in-ss">Cerrar sesion</button>
          </a>
        </div>
      </div>
    </div>

    <div class="row" >
      <div class="col-2 bg-dark">
        <nav class="nav flex-column pt-3">
          <a class="nav-link text-white" href="{{ action('TiendaController@Login', "$id") }}">Inicio</a>
          <a class="nav-link text-white tienda crs-pointer" id="tienda">Tienda</a>
          <ul class="nav flex-column bg-submenu " id="sub-tienda">
            <li class="nav-item">
              <a class="nav-link a" href="{{ action('TiendaController@Perfil', "$id") }}">Perfil</a>
            </li>
            <li class="nav-item">
              <a class="nav-link a" href="{{ action('TiendaController@ModificarPerfil', "$id") }}">Modificar</a>
            </li>
          </ul>
          <a class="nav-link text-white" href="#">Ventas</a>
          <a class="nav-link text-white crs-pointer" href="#" id="productos">Productos</a>
          <ul class="nav flex-column bg-submenu" id="sub-productos">
            <li class="nav-item">
              <a class="nav-link a" href="{{ action('TiendaController@AñadirProducto', $id) }}">Añadir</a>
            </li>
            <li class="nav-item">
              <a class="nav-link a" href="{{ action('TiendaController@ModificarProducto', $id) }}">Modificar</a>
            </li>
            <li class="nav-item">
              <a class="nav-link a" href="{{ action('TiendaController@EliminarProducto', $id) }}">Eliminar</a>
            </li>
          </ul>
          <a class="nav-link text-white" href="#">Valoraciones</a>
        </nav>
      </div>
      <div class="col-10" style="min-height: 90vh; margin-bottom: 60px;">
        @yield('content')
      </div>
    </div>
    
  </div>


  {{-- footer --}}
  <footer class="container-fluid text-white" style="background-color: #cecece; margin-top: -4px;">
    <div class="row mt-1">
      <div class="col-12">

      </div>
    </div>
    <div class="row m-2">
      <div class="col-12 d-flex justify-content-center">
        <i class="fab fa-facebook-square mr-3 rs"></i>
        <i class="fab fa-twitter mr-3 rs"></i>
        <i class="fab fa-instagram mr-3 rs"></i>
      </div>
    </div>
    <div class="row p-2">
      <div class="col-12 d-flex justify-content-center">
        <span>Sitio web creado por @Larrygbc.</span>
      </div>
    </div>
  </footer>


  <script src="http://baldu.com/public/js/jquery-3.3.1.min.js"></script>
  <script src="http://baldu.com/public/js/popper.js"></script>
  {{-- script material design --}}
  <script src="http://baldu.com/public/js/bootstrap-material-design.min.js"></script>
  {{-- Iniciar el script de bootstrap --}}
  <script>
    $(document).ready(function() { $('body').bootstrapMaterialDesign(); });
  </script>
  
  <script src="http://baldu.com/public/js/snackbar.js"></script>

  @yield('script')
</body>
</html>