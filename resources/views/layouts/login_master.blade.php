<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="icon" href="../../../public/imagen/favicon.ico">

  <!-- Material Design for Bootstrap fonts and icons -->
  <link rel="stylesheet" href="http://baldu.com/public/css/fuentes.css">

  <!-- Material Design for Bootstrap CSS -->
  <link rel="stylesheet" href="http://baldu.com/public/css/bootstrap-material-design.min.css">

  <!--iconos material design-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
  rel="stylesheet">

  <link rel="stylesheet" href="http://baldu.com/public/css/fontawesome-all.min.css">
  
  {{-- Mi css --}}
  <link rel="stylesheet" href="http://baldu.com/public/css/my-style.css">
  
  {{-- notificaciones snackbar --}}
  <link rel="stylesheet" href="http://baldu.com/public/css/material.css">


  <title>@yield('title')</title>
  @yield('css')
  @yield('meta')
</head>
<body>

 <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay">
  <header class="bmd-layout-header">
    <div class="navbar navbar-light bg-success d-flex justify-content-between">
      <div class="d-flex justify-content-between">
        <button class="navbar-toggler" type="button" data-toggle="drawer" data-target="#dw-p1">
          <span class="sr-only">Toggle drawer</span>
          <i class="material-icons">menu</i>
        </button>
        <div class="d-flex align-items-center ml-3">
          <h4 class="tt-baldu">
            <a href="{{ action('HomeController@getLogin', $S_usuario) }}">Baldu</a>
          </h4>
        </div>
      </div>

      {{-- buscador --}}
      <div class="buscador mx-auto" id="buscador">
        <form class="d-flex justify-content-between align-items-center" action="{{ action('HomeController@getbusqueda', $S_usuario) }}" method="get" id="form">
          <button class="btn bmd-btn-icon button-buscar fl-none" type="submit" >
            <i class="material-icons text-white" id="icon-buscar">search</i>
          </button>
          <span class="bmd-form-group ">
            <input class="input-buscar flex-column color-place-white" placeholder="Buscar..." type="text" id="input-buscar" name="search">
          </span>
          <button class="btn bmd-btn-icon button-buscar fl-none" type="reset">
            <i class="material-icons text-white" id="icon-close">close</i>
          </button>
        </form>
        <button class="btn btn-raised btn-in-ss" id="btn-filtro">filtros</button>
      </div>

      <div class="float-right d-flex justify-content-between">
        @foreach($usuario as $value)
        <img src="http://baldu.com/public/imagen/{{ $value->ruta }}" alt="" class="img-user" id="img-user">
        @endforeach
        <div class="box-user" id="box-user">
          @yield('box-user')
        </div>

        <button class="btn btn-raised btn-sm btn-in-cm ml-4" id="btn-carrito">
          <i class="material-icons">local_grocery_store</i>
        </button>
        <div class="cont-not" id="cont-not">
          <p class="contador-compra" id="contador">0</p>
        </div>
        <div class="box-carrito" id="carrito">
          @yield('carrito')
        </div>     
      </div>
    </div>
  </header>
  <div id="dw-p1" class="bmd-layout-drawer bg-faded">
    <header>
      <a class="navbar-brand">Perfil</a>
    </header>
    <ul class="list-group">
      <a class="list-group-item" href="{{ action('HomeController@Perfil', $S_usuario) }}">Cuenta</a>
      <a class="list-group-item" href="{{ action('HomeController@Compras', $S_usuario) }}">Compras</a>
      <a class="list-group-item" href="{{ action('HomeController@getLogout') }}">Salir</a>
    </ul>
  </div>
  <main class="bmd-layout-content" style="min-height: 90vh;">
    @yield('content')
  </main>
</div>

{{-- footer --}}
<footer class="container-fluid text-white" style="background-color: #cecece;">
  <div class="row mt-1">
    <div class="col-12">

    </div>
  </div>
  <div class="row m-2">
    <div class="col-12 d-flex justify-content-center">
      <i class="fab fa-facebook-square mr-3 rs"></i>
      <i class="fab fa-twitter mr-3 rs"></i>
      <i class="fab fa-instagram mr-3 rs"></i>
    </div>
  </div>
  <div class="row p-2">
    <div class="col-12 d-flex justify-content-center">
      <span>Sitio web creado por @Larrygbc.</span>
    </div>
  </div>
</footer>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="http://baldu.com/public/js/jquery-3.3.1.min.js"></script>
<script src="http://baldu.com/public/js/popper.js"></script>
{{-- script material design --}}
<script src="http://baldu.com/public/js/bootstrap-material-design.min.js"></script>
{{-- Iniciar el script de bootstrap --}}
<script>
  $(document).ready(function() { $('body').bootstrapMaterialDesign(); });
</script>
<script src="http://baldu.com/public/js/snackbar.js"></script>
{{-- Script baldu --}}
@yield('script')

</body>
</html>




