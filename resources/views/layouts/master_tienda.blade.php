<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="icon" href="http://baldu.com/public/imagen/favicon.ico">

  <!-- Material Design for Bootstrap fonts and icons -->
  <link rel="stylesheet" href="http://baldu.com/public/css/fuentes.css">

  <!-- Material Design for Bootstrap CSS -->
  <link rel="stylesheet" href="http://baldu.com/public/css/bootstrap-material-design.min.css">

  <!--iconos material design-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
  rel="stylesheet">

  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <link rel="stylesheet" href="http://baldu.com/public/css/my-style.css">

  <link rel="stylesheet" href="http://baldu.com/public/css/material.css">
  



  <title>@yield('title')</title>
</head>
<body>
  @yield('content')


  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="http://baldu.com/public/js/jquery-3.3.1.min.js"></script>
  <script src="http://baldu.com/public/js/popper.js"></script>
  <script src="http://baldu.com/public/js/snackbar.js"></script>
  {{-- script material design --}}
  <script src="http://baldu.com/public/js/bootstrap-material-design.min.js"></script>
  {{-- Iniciar el script de bootstrap --}}
  <script>
    $(document).ready(function() { $('body').bootstrapMaterialDesign(); });
  </script>
  
  {{-- Script baldu --}}
  @yield('script')
</body>
</html>