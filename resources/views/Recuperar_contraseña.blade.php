
@extends('layouts/bootstrap')

@section('title')
Baldu
@endsection

@section('content')

<div class="container ">
	<div class="row" style="height: 100vh;">
		<div class="col-12 text-center d-flex justify-content-center align-items-center">
			<div class="box-in-ss mx-auto" style="width: 400px;">
				<h3 class="text-left">Baldu</h3>
				<p class="text-left mb-4 mt-1">Olvidaste tu contraseña, no te preocupes aqui estamos para ayudarte</p>
				<i class="material-icons logo-in-ss">lock</i>
				<form action="{{ action('ValidarController@Contraseña') }}" method="post" id="form-rec-usuario">
					{{ csrf_field() }}
					<div class="form-group text-left">
						<label for="email" class="bmd-label-floating">Email o usuario</label>
						<input type="text" class="form-control" id="email" style="display: unset;" name="email">
						<div class="error" id="Er-rec-email" style="display: none;"></div>
					</div>
					<div class="form-group text-left">
						<label for="Password" class="bmd-label-floating">Nueva ontraseña</label>
						<input type="password" class="form-control" id="password" style="display: unset;" name="Password1">
						<div class="error" id="Er-rec-pass1" style="display: none;"></div>
					</div>
					<div class="form-group text-left">
						<label for="Password" class="bmd-label-floating">Repetir nueva contraseña</label>
						<input type="password" class="form-control" id="rec-password" style="display: unset;" name="Password2">
						<div class="error" id="Er-rec-pass2" style="display: none;"></div>
					</div>
					<div class="error" id="Er-rec-igual" style="display: none;"></div>
					
					<div class="form-group d-flex justify-content-between align-items-center mt-3">
						<input type="submit" class="btn btn-raised btn-in-ss" value="Aceptar">
					</div>
				</form>
				<div class="error">
					<p>{{ $mensaje }}</p>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('script')
<script src="http://baldu.com/public/js/index.js"></script>
@endsection
