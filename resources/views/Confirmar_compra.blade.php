@extends('layouts/login_master')

@section('title')
Baldu
@endsection

@section('css')
<link rel="stylesheet" href="http://baldu.com/public/css/index.css">
@endsection

@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection


{{-- contenedor identificacion del usuario --}}
@section('box-user')
<div class="">
  @foreach($usuario as $value)
  <img src="http://baldu.com/public/imagen/{{ $value->ruta }}" alt="" class="box-user-img">
  @endforeach
</div>
<p class="text-center n-ss" id='User-login'>{{ $S_usuario }}</p>
<p class="text-center e-ss" id='User-nombre'>{{ $S_nombre }}</p>
<p class="text-center e-ss" id="User-email">{{ $S_email }}</p>
<div class="d-flex justify-content-between m-4">
	<a href="">
		<button class="btn  btn-raised btn-sm btn-in-ss">Mi cuenta</button>
	</a>
	<a href="{{ action('HomeController@getLogout') }}">
		<button class="btn btn-raised btn-sm btn-in-ss">Cerrar sesion</button>
	</a>
</div>
@endsection

{{-- contenedor del carrito de compra --}}
@section('carrito')
<div class="text-center mt-2">
	<h4>Carrito de compra</h4>
	<h6>Productos añadidos</h6>
</div>
<div class="cont-compra">
	<div id="div-carrito">
	</div>
	<div class="d-flex justify-content-between">
		<p class="font-weight-bold" id="precio-text">Total</p>
		<p class="font-weight-bold" id="precio-total">0€ </p>
	</div>
</div>
<div class="d-flex justify-content-center m-2">
	<a href="{{ action('HomeController@getConfirmarCompra', $S_usuario) }}"><button class="btn btn-raised btn-sm btn-in-ss" id="btn-conf-compra">comprar</button></a>
</div>
@endsection

{{-- contenido pincipal 'Body' --}}
@section('content')
<div class="container mt-5"  style='min-height: 100vh;'>
	<h1 class="text-center">Carrito de compra</h1>
	<div class="accordion">
		<div class="card">
			<div class="card-header">
				<h5 class="mb-0">
					<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						productos
					</button>
				</h5>
			</div>
			<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
				<div class="card-body">
					<div class="row">
						<div class="col-12">
							<h3 class="text-center p-3">Productos seleccionados</h3>
							<table class="table">
								<thead class="thead-light">
									<tr>
										<th scope="col">Id</th>
										<th scope="col">Producto</th>

										<th scope="col">Precio</th>
									</tr>
								</thead>
								<tbody>
									@php
									$total = 0;
									$contador = 0;
									@endphp
									@if($productos != null)
									@foreach($productos as $value)
									@php
									$nombre = $value->nombre;
									$cantidad = $value->precio;
									$total+=$cantidad;
									@endphp
									<tr>
										<th scope='row'>{{ $value->id }}</th>
										<td>{{ $value->nombre }}</td>
										<td>{{ $var = $value->precio }}€</td>
									</tr>
									@endforeach
									@endif
									<tr>
										<th scope='row'>Total</th>
										<td></td>
										<td>{{ $total }}€</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-header" id="headingTwo">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Entrega</button>
				</h5>
			</div>
			<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
				<div class="card-body">
					<div class="row">
						<div class="col-12">
							<h3 class="text-center p-3">Elije el metodo de entrega</h3>
							<form action="">
								<div class="radio">
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
										Recogida en tienda
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
										Entrega a domicilio (Glovo)									
									</label>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingTwo">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">Pago</button>
					</h5>
				</div>
				<div id="collapse3" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
					<div class="card-body">
						<div class="row">
							<div class="col-12">
								<h3 class="text-center p-3">Confirmar Pago</h3>
								<div id="pagando" style="display: none;">
									<div class="preloader mx-auto"></div>
									<p class="text-center">Pagando...</p>
								</div>
								<div id="confirmar-pago" style="display: none;">
									<i class='fa fa-check-circle-o pr-3'></i>
									<p class="text-center">Se ha Pagado correctamente</p>
								</div>
								<button class="btn btn-raised btn-sm btn-in-ss mx-auto" style="display: flex;" id="pagar">Pagar</button>

							</div>
						</div>
					</div>
				</div>
			</div>




		</div>


	</div>

</div>
@endsection

@section('script')
<script src="http://baldu.com/public/js/index.js"></script>
@endsection