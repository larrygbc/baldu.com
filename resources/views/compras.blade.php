@extends('layouts/login_master')

@section('title')
Baldu
@endsection

@section('css')
<link rel="stylesheet" href="http://baldu.com/public/css/index.css">
@endsection

@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection


{{-- contenedor identificacion del usuario --}}
@section('box-user')
<div class="">
  @foreach($usuario as $value)
  <img src="http://baldu.com/public/imagen/{{ $value->ruta }}" alt="" class="box-user-img">
  @endforeach
</div>
<p class="text-center n-ss" id='User-login'>{{ $S_usuario }}</p>
<p class="text-center e-ss" id='User-nombre'>{{ $S_nombre }}</p>
<p class="text-center e-ss" id="User-email">{{ $S_email }}</p>
<div class="d-flex justify-content-between m-4">
  <a href="{{ action('HomeController@Perfil', $S_usuario) }}">
    <button class="btn  btn-raised btn-sm btn-in-ss">Mi cuenta</button>
  </a>
  <a href="{{ action('HomeController@getLogout') }}">
    <button class="btn btn-raised btn-sm btn-in-ss">Cerrar sesion</button>
  </a>
</div>
@endsection

{{-- contenedor del carrito de compra --}}
@section('carrito')
<div class="text-center mt-2">
  <h4>Carrito de compra</h4>
  <h6>Productos añadidos</h6>
</div>
<div class="cont-compra">
  <div id="div-carrito">
  </div>
  <div class="d-flex justify-content-between">
    <p class="font-weight-bold" id="precio-text">Total</p>
    <p class="font-weight-bold" id="precio-total">0€ </p>
  </div>
</div>
<div class="d-flex justify-content-center m-2">
  <a href="{{ action('HomeController@getConfirmarCompra', $S_usuario) }}"><button class="btn btn-raised btn-sm btn-in-ss" id="btn-conf-compra">comprar</button></a>
</div>
@endsection

{{-- contenido pincipal 'Body' --}}
@section('content')

<div class="container" style="min-height: 100vh;">
  <div class="row">
    <div class="col-12">
      <h1 class="text-center mt-5 mb-5">Compras realizadas</h1>
      <div>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">Id de la compra</th>
              <th scope="col">Fecha de la compra</th>
              <th scope="col">Total</th>
            </tr>
          </thead>
          <tbody>
            @foreach($compras as $key => $value)
            <tr>
              <th scope="row">
                <a class="btn btn-primary id_compra" href="{{ action('HomeController@DetalleCompra', [$S_usuario, $value->id]) }}">
                  {{ $value->id }}
                </a>
              </th>
              <td>{{ $value->fecha_compra }}</td>
              <td>{{ $value->importe_total }}€</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection

@section('script')
<script src="http://baldu.com/public/js/index.js"></script>
@endsection