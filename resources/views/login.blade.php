@extends('layouts/login_master')

@section('title')
Baldu
@endsection

@section('css')
<link rel="stylesheet" href="http://baldu.com/public/css/index.css">
@endsection

@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection


{{-- contenedor identificacion del usuario --}}
@section('box-user')
<div class="">
	@foreach($usuario as $value)
	<img src="http://baldu.com/public/imagen/{{ $value->ruta }}" alt="" class="box-user-img">
	@endforeach
</div>
<p class="text-center n-ss" id='User-login'>{{ $S_usuario }}</p>
<p class="text-center e-ss" id='User-nombre'>{{ $S_nombre }}</p>
<p class="text-center e-ss" id="User-email">{{ $S_email }}</p>
<div class="d-flex justify-content-between m-4">
	<a href="{{ action('HomeController@Perfil', $S_usuario ) }}">
		<button class="btn  btn-raised btn-sm btn-in-ss">Mi cuenta</button>
	</a>
	<a href="{{ action('HomeController@getLogout') }}">
		<button class="btn btn-raised btn-sm btn-in-ss">Cerrar sesion</button>
	</a>
</div>
@endsection

{{-- contenedor del carrito de compra --}}
@section('carrito')
<div class="text-center mt-2">
	<h4>Carrito de compra</h4>
	<h6>Productos añadidos</h6>
</div>
<div class="cont-compra">
	<div id="div-carrito">
	</div>
	<div class="d-flex justify-content-between">
		<p class="font-weight-bold" id="precio-text">Total</p>
		<p class="font-weight-bold" id="precio-total">0€ </p>
	</div>
</div>
<div class="d-flex justify-content-center m-2">
	<a href="{{ action('HomeController@getConfirmarCompra', $S_usuario) }}"><button class="btn btn-raised btn-sm btn-in-ss" id="btn-conf-compra">comprar</button></a>
</div>
@endsection

{{-- contenido pincipal 'Body' --}}
@section('content')
<div class="container dnone" id="filtro">
	<div class="row mt-1" id="row-filtro">
		<div class="col-12 bg-success filtro">
			<form action="{{ action('HomeController@getbusqueda', $S_usuario) }}" method="get">
				<input type="hidden" value="filtro" name="filtro">
				<div class="form-row">
					<div class="form-group col-md-4 col-12">
						<label for="">Buscador</label>
						<input class="form-control" placeholder="Buscar..." type="text" id="input-buscar" name="search" style="height: 41px;">
					</div>
					<div class="form-group col-12 col-md-4">
						<label for="" class="bmd-label-floating">Categoria</label>
						<select name="sel_cat" id="id-cat" class="form-control" name="cat">
						</select>
					</div>
					<div class="form-group col-12 col-md-4">
						<label for="">Codigo postal</label>
						<select name="sel_cod" id="cod_postal" class="form-control">
						</select>
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-12 col-md-4">
						<label for="">Barrio</label>
						<select name="sel_barrio" id="barrio" class="form-control">
						</select>
					</div>
					<div class="form-group col-md-4 col-12">
						<label for="">Min</label>
						<input type="text" id="min" class="form-control" name="sel_min" style="height: 41px;" value="0">
					</div>
					<div class="form-group col-md-4 col-12">
						<label for="">Max</label>
						<input type="text" id="max" class="form-control" name="sel_max"  style="height: 41px;" value=500>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-12 text-center d-flex align-items-center justify-content-center mt-3">
						<button id="btn-form-cat" class="btn btn-raised btn-sm btn-in-ss ">Buscar</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
{{-- Contenedor de los productos --}}
<div class="container">
	<div class="card-columns mt-4 mx-auto" id="row_card">
		@foreach($productos as $key => $value)
		<div class='tarjeta mb-3'>
			<input type="hidden" name='id-producto' value='{{ $value->id }}'>
			<div>
				@if(empty($value->ruta))
				<img src="" alt='' class="foto-producto"/>
				@else
				<img src="http://baldu.com/public/imagen/{{ $value->ruta }}" alt='' class='foto-producto'/>
				@endif
			</div>
			<div class='card-body' >
				<div class='d-flex justify-content-between align-items-end' id='card-body'>
					<h5 class='card-titulo' id=''>{{ $value->nombre }}</h5>
					<p class='card-precio' id=''>{{ $value->precio }}€</p>
				</div>
				<p class='card-subtitulo'>{{ $value->descripcion }}</p>
			</div>
			<div class='card-footer'>
				<button type='button' class='btn btn-raised btn-sm btn-in-ss d-flex justify-content-center align-items-center añadir-carrito' >
					<i class='material-icons'>add_shopping_cart</i>
				</button>
			</div>
		</div>
		@endforeach
	</div>
</div>

@endsection

@section('script')
<script src="http://baldu.com/public/js/index.js"></script>
@endsection