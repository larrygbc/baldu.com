@extends('layouts/login_master')

@section('title')
Baldu
@endsection

@section('css')
<link rel="stylesheet" href="http://baldu.com/public/css/index.css">
@endsection

@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection


{{-- contenedor identificacion del usuario --}}
@section('box-user')
<div class="">
  @foreach($usuario as $value)
  <img src="http://baldu.com/public/imagen/{{ $value->ruta }}" alt="" class="box-user-img">
  @endforeach
</div>
<p class="text-center n-ss" id='User-login'>{{ $S_usuario }}</p>
<p class="text-center e-ss" id='User-nombre'>{{ $S_nombre }}</p>
<p class="text-center e-ss" id="User-email">{{ $S_email }}</p>
<div class="d-flex justify-content-between m-4">
  <a href="{{ action('HomeController@Perfil', $S_usuario) }}">
    <button class="btn  btn-raised btn-sm btn-in-ss">Mi cuenta</button>
  </a>
  <a href="{{ action('HomeController@getLogout') }}">
    <button class="btn btn-raised btn-sm btn-in-ss">Cerrar sesion</button>
  </a>
</div>
@endsection

{{-- contenedor del carrito de compra --}}
@section('carrito')
<div class="text-center mt-2">
  <h4>Carrito de compra</h4>
  <h6>Productos añadidos</h6>
</div>
<div class="cont-compra">
  <div id="div-carrito">
  </div>
  <div class="d-flex justify-content-between">
    <p class="font-weight-bold" id="precio-text">Total</p>
    <p class="font-weight-bold" id="precio-total">0€ </p>
  </div>
</div>
<div class="d-flex justify-content-center m-2">
  <a href="{{ action('HomeController@getConfirmarCompra', $S_usuario) }}"><button class="btn btn-raised btn-sm btn-in-ss" id="btn-conf-compra">comprar</button></a>
</div>
@endsection

{{-- contenido pincipal 'Body' --}}
@section('content')

<div class="container" style="min-height: 100vh;">
  <div class="row">

    <div class="col-12 mt-3">

      <h1 class="text-center mt-5 mb-5">Perfil</h1>
      @foreach($usuario as $value)
      <form action="{{ action('HomeController@ModificarPerfil', $S_usuario) }}" id="form-perfil" method="post" enctype="multipart/form-data" >
        {{ csrf_field() }}
        <div class="text-center image mb-5">
          <img src="http://baldu.com/public/imagen/{{ $value->ruta }}" alt="" class="foto sombra-foto" >
        </div>
        <div class="form-group mt-2">
          <label for="exampleFormControlFile1">Foto de perfil</label>
          <input type="file" class="form-control-file" id="pfl-imagen" name='imagen'>
          <div class="error" id="Er-pfl-imagen"></div>
        </div>
        <div class="form-group">
          <label for="">Nombre</label>
          <input type="text" class="form-control" value="{{ $value->nombre }}" name="pfl_nombre" id="pfl-nombre">
          <div class="error" id="Er-pfl-nombre"></div>
        </div>
        <div class="form-group">
          <label for="">Apellidos</label>
          <input type="text" class="form-control" value="{{ $value->apellidos }}" name="pfl_apellidos" id="pfl-apellidos">
          <div class="error" id="Er-pfl-apellidos"></div>
        </div>
        <div class="form-group">
          <label for="">Telefono</label>
          <input type="tel" class="form-control" value="{{ $value->telefono }}" name="pfl_tel" id="pfl-tel">
          <div class="error" id="Er-pfl-tel"></div>
        </div>
        <div class="form-group">
          <label for="">Fecha de nacimiento</label>
          <input type="date" class="form-control" value="{{ $value->fecha_nacimiento }}" name="pfl_date" id="pfl-date">
          <div class="error" id="Er-pfl-date"></div>
        </div>
        
        <div class="form-group">
          <label for="">Direccion</label>
          <input type="text" class="form-control" value="{{ $value->direccion }}" name="pfl_direccion" id="pfl-direccion">
          <div class="error" id="Er-pfl-direccion"></div>
        </div>
        
        <div class="form-group mb-2">
          @php
          $clave = $value->codigo_postal;
          @endphp
          <label for="">Codigo postal</label>
          <select class="form-control" name="pfl_cod" id="pfl-cod">
            @foreach ($codigo_postal as $value)
            <option value="{{ $value->codigo_postal }}" 
              @if ($clave == $value->codigo_postal) 
              selected="selected"
              @endif
              >
              {{ $value->codigo_postal }}
            </option>
            @endforeach
          </select>
        </div>
        
        <div class="form-group">
          <input type="submit" class="btn btn-raised btn-in-ss" value='Aceptar'>
        </div>
      </form>
      @endforeach
      <div class="alert alert-primary mt-5" role="alert">
        Si desea modificar sus datos sobreescriba los datos actuales y acepte
      </div>
      <div class="alert alert-danger mt-5" role="alert">
        <h4>Darse de baja de baldu</h4>
        <p>Al darse de baja se borrara toda la informacion relacionada con tu tienda</p>
        <button type="button" class="btn btn-raised btn-in-ss" data-toggle="modal" data-target="#exampleModalCenter">Baja</button>
      </div>
    </div>
  </div>
</div>

{{-- modal para dar de baja --}}
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Darse de baja</h5>
      </div>
      <form action="{{ action('HomeController@BorrarCuenta', $S_usuario) }}" method="post">
        {{ csrf_field() }}
        <div class="modal-body">
          <i class="material-icons d-flex justify-content-center" style="font-size: 110px; color: #80808094;">mood_bad</i>
          <p class="d-flex justify-content-center mt-3 text-danger" style="font-size: 16px;">Confirmar la baja de en baldu.com</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Aceptar</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="http://baldu.com/public/js/index.js"></script>
@endsection