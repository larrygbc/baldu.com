$(function(){

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$.ajax({
		url: 'http://baldu.com/TiendaCodigoPostal',
		type: 'POST',
		dataType: 'json',
	})
	.done(function(data) {
		for (var i = 0; i < data.length; i++) {
			var option = "<option id='"+ data[i]['id'] +"'>" + data[i]['codigo_postal'] + "</option>";
			$('#Tnd-cod').append(option);
		};
	})

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$.ajax({
		url: 'http://baldu.com/TiendaBarrio',
		type: 'POST',
		dataType: 'json',
	})
	.done(function(data) {
		for (var i = 0; i < data.length; i++) {
			var option = "<option id='"+ data[i]['id'] +"'>" + data[i]['nombre'] + "</option>";
			$('#Tnd-barrio').append(option);
		};
	})


	var correcto = true;

	$('#btn-Tnd-conf').click(function(event) {

		$('#Tnd-form').submit(function(event) {
			return false;
		});

		var nombre = $('#Tnd-nombre').val();
		if(nombre.length == 0 ){
			$('#Error-Tnombre').show();
			$('#Error-Tnombre').text('Este campo no puede estar vacio');
			correcto = false;
		}else{
			$('#Error-Tnombre').hide();
			correcto = true;
		}

		if($('#Tnd-email').val().length == 0 ){
			$('#Error-Temail').show();
			$('#Error-Temail').text('Este campo no puede estar vacio');
			correcto = false;
		}else if($('#Tnd-email').val().length > 0){

			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url: '/ValidarTiendaEmail',
				type: 'POST',
				dataType: 'json',
				data: {
					param1: $('#Tnd-email').val()
				},
			})
			.done(function(data) {
				if(data == true){
					$('#Error-Temail').show();
					$('#Error-Temail').text('Este email ya esta registrado');
					correcto = false;
				}else{
					$('#Error-Temail').hide();
					correcto = true;
				}
			})
		}else{
			$('#Error-Temail').hide();
			correcto = true;
		}

		if($('#Tnd-password').val().length == 0){
			$('#Error-Tpass').show();
			$('#Error-Tpass').text('Este campo no puede estar vacio');
			correcto = false;	
		}else if($('#Tnd-password').val().length <= 5 ){
			$('#Error-Tpass').show();
			$('#Error-Tpass').text('La contraseña debe tener minimo 5 digitos');	
			correcto = false;
		}else{
			$('#Error-Tpass').hide();
			correcto = true;
		}

		if ($('#Tnd-cif').val().length == 0) {
			$('#Error-Tcif').show();
			$('#Error-Tcif').text('Este campo no puede estar vacio');
			correcto = false;	
		}else if($('#Tnd-cif').val().length == 9){
			$('#Error-Tcif').hide();
			correcto = false;
		}else{
			$('#Error-Tcif').show();
			$('#Error-Tcif').text('El numero CIF se conmpone de 9 digitos');
			correcto = true;
		}

		if($('#Tnd-cod option:selected').attr('id') == 0 ){
			$('#Error-Tcod').show();
			$('#Error-Tcod').text('Este campo no puede estar vacio');
			correcto = false;	
		}else{
			$('#Error-Tcod').hide();
			correcto = true;
		}

		if($('#Tnd-barrio').val().length == 0){
			$('#Error-Tbarrio').show();
			$('#Error-Tbarrio').text('Este campo no puede estar vacio');
			correcto = false;
		}else{
			$('#Error-Tbarrio').hide();
			correcto = true;
		}

		if($('#Tnd-direccion').val().length == 0){
			$('#Error-Tdireccion').show();
			$('#Error-Tdireccion').text('Este campo no puede estar vacio');
			correcto = false;
		}else{
			$('#Error-Tdireccion').hide();
			correcto = true;
		}

		if($('#Tnd-tel').val().length == 0){
			$('#Error-Ttel').show();
			$('#Error-Ttel').text('Este campo no puede estar vacio');
			correcto = false;
		}else if($('#Tnd-tel').val().length != 9){
			$('#Error-Ttel').show();
			$('#Error-Ttel').text('EL telefono debe tener 9 digitos');
			correcto = false;
		}else{
			$('#Error-Ttel').hide();
			correcto = true;
		}

		if (correcto) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url: '/resgistrotienda=ok',
				type: 'POST',
				dataType: 'json',
				data: {
					param1: $('#Tnd-nombre').val(),
					param2: $('#Tnd-email').val(),
					param3: $('#Tnd-password').val(),
					param4: $('#Tnd-cif').val(),
					param5: $('#Tnd-cod option:selected').attr('id'),
					param6: $('#Tnd-barrio option:selected').attr('id'),
					param7: $('#Tnd-direccion').val(),
					param8: $('#Tnd-tel').val()
				},
			})
			.done(function(data) {
				if (data) {
					var options =  {
						content: "Su tienda se ha registrado correctamente", 
						style: "snackbar",
						timeout: 1500
					}
					$.snackbar(options);

					setTimeout("redireccionarTienda()", 1800);
				}
			})
		};
	});	
})

function redireccionarTienda(){
	window.location = "http://baldu.com/admin/iniciar-sesion";
}