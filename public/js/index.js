var Tproductos = 0;

$(function(){

	//efectos al buscador
	$('#input-buscar').focus(function(event) {
		$('#input-buscar').removeClass('color-place-white');
		$('#buscador').addClass('bg-white');
		$('#icon-buscar').addClass('icon-buscar');
		$('#icon-close').addClass('icon-buscar');
		$('#input-buscar').addClass('color-place-grey');
	});

	$('#input-buscar').focusout(function(event) {
		$('#buscador').removeClass('bg-white');
		$('#input-buscar').removeClass('color-place-grey');
		$('#buscador').addClass('buscador');
		$('#input-buscar').addClass('color-place-white');
		$('#icon-buscar').removeClass('icon-buscar');
		$('#icon-close').removeClass('icon-buscar');
	});

	//cargar categorias en el filtro
	categorias();

	//busquedas
	$('#btn-filtro').click(function(event) {
		$('#buscador').hide();
		$('#filtro').show();
	});


	//mostrar caja de perfil del usuario
	$('#img-user').click(function(event) {
		$('#carrito').hide();
		$('#box-user').toggle();
	});	

	//añadir productos al carrito de compra
	$('.añadir-carrito').click(function(event) {
		
		$('#div-carrito').empty();
		var id_producto = $(this).parents('.tarjeta').children('input').val();
		var login = $('#User-login').text();	
		//aqui llamos a la funcion para enseñar los producto en el carrito
		var respuesta = Añadir_al_Carrito(id_producto);
		var total = 0;
		for (var i = 0; i < respuesta.length; i++) {
			var Add = 
			"<div class='d-flex justify-content-between'>" +
			"<div class='d-flex justify-content-between pedido' >" +
			"<input type='hidden' value='" + respuesta[i]['id'] + "'/>"+
			"<p style='width:160px;'>" + respuesta[i]['nombre'] + "</p>"+ 
			"<p style='text-align: right; width: 40px;'>" + respuesta[i].precio + "€</p>"+ 
			"</div>" +
			"<button class='borrar-producto car-close'>" +
			"<i class='material-icons'>close</i>" +
			"</button>" +
			"</div>";

			$('#div-carrito').append(Add);

			total += respuesta[i]['precio'];
		}
		//precio total de los productos
		$('#precio-total').text(total + "€");

		//eliminar productos del carrito de compra
		$('.car-close').click(function(event) {

			$('#div-carrito').empty();

			var Id_delete = $(this).siblings('.pedido').children('input').val();

			var prod_delete = EliminarDelCarrito(Id_delete);

			var Dtotal = 0;
			//aqui enseñamos el carrito de compra
			for (var i = 0; i < prod_delete.length; i++) {
				var DAdd = 
				"<div class='d-flex justify-content-between'>" +
				"<div class='d-flex justify-content-between pedido' >" +
				"<input type='hidden' value='" + prod_delete[i]['id'] + "'/>"+
				"<p style='width:160px;'>" + prod_delete[i]['nombre'] + "</p>"+ 
				"<p style='text-align: right; width: 40px;'>" + prod_delete[i].precio + "€</p>"+ 
				"</div>" +
				"<button class='borrar-producto car-close'>" +
				"<i class='material-icons'>close</i>" +
				"</button>" +
				"</div>";

				$('#div-carrito').append(DAdd);

				Dtotal += prod_delete[i]['precio'];
			}

			$('#precio-total').text(Dtotal + "€");

			location.reload(true);

		});
	});

	//boton carrito de compra
	$('#btn-carrito').click(function(event) {
		$('#box-user').hide();
		$('#carrito').toggle();

		var respuesta = CarroGuardado();

		var total = 0;

		if($('.pedido').length == 0){
			//enseñamos los productos añadidos en el carrito
			for (var i = 0; i < respuesta.length; i++) {
				var Add = 
				"<div class='d-flex justify-content-between'>" +
				"<div class='d-flex justify-content-between pedido' >" +
				"<input type='hidden' value='" + respuesta[i]['id'] + "'/>"+
				"<p style='width:160px;'>" + respuesta[i]['nombre'] + "</p>"+ 
				"<p style='text-align: right; width: 40px;'>" + respuesta[i].precio + "€</p>"+ 
				"</div>" +
				"<button class='borrar-producto car-close'>" +
				"<i class='material-icons'>close</i>" +
				"</button>" +
				"</div>";

				$('#div-carrito').append(Add);

				total += respuesta[i]['precio'];
			}

			$('#precio-total').text(total + "€");


			//eliminamos productos del carrito de compra
			$('.car-close').click(function(event) {

				$('#div-carrito').empty();

				var Id_delete = $(this).siblings('.pedido').children('input').val();
				
				var prod_delet = EliminarDelCarrito(Id_delete);

				var Dtota = 0;

				for (var i = 0; i < prod_delet.length; i++) {
					var DAd = 
					"<div class='d-flex justify-content-between'>" +
					"<div class='d-flex justify-content-between pedido' >" +
					"<input type='hidden' value='" + prod_delet[i]['id'] + "'/>"+
					"<p style='width:160px;'>" + prod_delet[i]['nombre'] + "</p>"+ 
					"<p style='text-align: right; width: 40px;'>" + prod_delet[i].precio + "€</p>"+ 
					"</div>" +
					"<button class='borrar-producto car-close'>" +
					"<i class='material-icons'>close</i>" +
					"</button>" +
					"</div>";

					$('#div-carrito').append(DAd);

					Dtota += prod_delet[i]['precio'];
				}

				$('#precio-total').text(Dtota + "€");
				location.reload(true);

			});
		}
	});

	//alerta cuando quiere añadir productos sin iniciar sesion
	$('.card-footer button').click(function(event) {
		var Confirm_add = $('#User-login').text();
		if(Confirm_add.length > 0){

		}else{
			var options =  {
				content: "Primero debe iniciar sesion", 
				style: "snackbar",
				timeout: 1500
			}
			$.snackbar(options);
		}
	});

	//realizar la compra
	$('#pagar').click(function(event) {
		$('#pagando').show('fast', function() {
			setTimeout(function(){ 
				$('#pagando').hide();
				$('#confirmar-pago').show();
				var array = CarroGuardado();
				ConfirmarCompra(array, $('#User-login').text());
				setTimeout(function(){ 
					location.href = "http://baldu.com/auth/" + $('#User-login').text();
				}, 1500);
			}, 3000);

		});
	});

	//validar formulario de perfil
	$('#form-perfil').submit(function(event) {
		var correcto = true;

		var foto = $('#pfl-imagen').val();

		var imagen = foto.substr(-3, foto.indexOf('.'));

		if(imagen != 'jpg' && imagen != 'png' && imagen !='' ){
			correcto = false;
			$('#Er-pfl-imagen').show();
			$('#Er-pfl-imagen').text('Este archivo no es una imagen');
		}else{
			$('#Er-pfl-imagen').hide();
		}

		if($('#pfl-nombre').val().length == 0){
			correcto = false;
			$('#Er-pfl-nombre').show();
			$('#Er-pfl-nombre').text('Este campo no puede estar vacio');
		}else{
			$('#Er-pfl-nombre').hide();
		}

		if($('#pfl-direccion').val().length == 0){
			correcto = false;
			$('#Er-pfl-direccion').show();
			$('#Er-pfl-direccion').text('Este campo no puede estar vacio');
		}else{
			$('#Er-pfl-direccion').hide();
		}

		if($('#pfl-tel').val().length == 0){
			correcto = false;
			$('#Er-pfl-tel').show();
			$('#Er-pfl-tel').text('Este campo no puede estar vacio');
		}else{
			$('#Er-pfl-tel').hide();
		}

		if($('#pfl-date').val().length == 0){
			correcto = false;
			$('#Er-pfl-date').show();
			$('#Er-pfl-date').text('Este campo no puede estar vacio');
		}else{
			$('#Er-pfl-date').hide();
		}

		if($('#pfl-cod').val().length == 0){
			correcto = false;
			$('#Er-pfl-cod').show();
			$('#Er-pfl-cod').text('Este campo no puede estar vacio');
		}else{
			$('#Er-pfl-cod').hide();
		}

		if (correcto) {
			return true;
		}else{
			return false;
		}
	});

	//validar formulario de inicio de sesion
	$('#form-inicio-sesion').submit(function(event) {
		var correcto = true;
		if($('#email').val().length == 0){
			correcto =  false;
			$('#Er-ini-email').show();
			$('#Er-ini-email').text('Este campo no puede estar vacio')
		}else{
			$('#Er-ini-email').hide();
		}

		if($('#password').val().length == 0){
			correcto =  false;
			$('#Er-ini-pass').show();
			$('#Er-ini-pass').text('Este campo no puede estar vacio')
		}else{
			$('#Er-ini-pass').hide();
		}

		if (correcto) {
			return true;
		}else{
			return false;
		}
	});

	//validar formulario de inicio de sesion de la tienda
	$('#form-ini-tnd').submit(function(event) {
		var correcto = true;
		if($('#email').val().length == 0){
			correcto =  false;
			$('#Er-ini-email').show();
			$('#Er-ini-email').text('Este campo no puede estar vacio')
		}else{
			$('#Er-ini-email').hide();
		}

		if($('#password').val().length == 0){
			correcto =  false;
			$('#Er-ini-pass').show();
			$('#Er-ini-pass').text('Este campo no puede estar vacio')
		}else{
			$('#Er-ini-pass').hide();
		}

		if (correcto) {
			return true;
		}else{
			return false;
		}
	});

	// validar formulario de recuperacion de contraseña del usuario
	$('#form-rec-usuario').submit((function(event) {
		var correcto = true;
		
		if($('#email').val().length == 0){
			correcto =  false;
			$('#Er-rec-email').show();
			$('#Er-rec-email').text('Este campo no puede estar vacio')
		}else{
			$('#Er-rec-email').hide();
		}

		var igual = true;
		if($('#password').val().length == 0){
			correcto =  false;
			$('#Er-rec-pass1').show();
			$('#Er-rec-pass1').text('Este campo no puede estar vacio');
		}else{
			$('#Er-rec-pass1').hide();
		}

		if($('#rec-password').val().length == 0){
			correcto =  false;
			$('#Er-rec-pass2').show();
			$('#Er-rec-pass2').text('Este campo no puede estar vacio');
		}else{
			$('#Er-rec-pass2').hide();
		}

		if( $('#rec-password').val() != $('#password').val() ){
			correcto = false;
			$('#Er-rec-igual').show();
			$('#Er-rec-igual').text('Las contraseñas no son iguales');
		}

		if (correcto) {
			return true;
		}else{
			return false;
		}
	}));
});

/*Funciones*/
function CompraProductos(){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	})
	return JSON.parse($.ajax({
		url: "/carrito",
		type: 'post',
		dataType: 'json',
		data:{
			param: valor
		},
		async: false,
		success: function(data){
			return data;
		}
	}).responseText);
}

function categorias(){

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$.ajax({
		url: '/filtro',
		type: 'POST',
	})
	.done(function(res) {
		var cat;
		for (var i = 0; i < res.categorias.length; i++) {
			cat = "<option value=" + res.categorias[i]['id'] + ">" + res.categorias[i]['categoria']+ "</option>";
			$('#id-cat').append(cat);
		};

		var cod;
		for (var i = 0; i < res.codigo_postal.length; i++) {
			cod = "<option value=" + res.codigo_postal[i]['id']  + ">" + res.codigo_postal[i]['codigo_postal']+ "</option>";
			$('#cod_postal').append(cod);
		};

		var barrio;
		for (var i = 0; i < res.barrio.length; i++) {
			barrio = "<option value="+ res.barrio[i]['id']  +">" + res.barrio[i]['nombre']+ "</option>";
			$('#barrio').append(barrio);
		};		
	})	
}

function Añadir_al_Carrito(valor){
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	})
	return JSON.parse($.ajax({
		url: "/carrito",
		type: 'post',
		dataType: 'json',
		data:{
			param: valor
		},
		async: false,
		success: function(data){
			return data;
		}
	}).responseText);
}

function EliminarDelCarrito(valor){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	})
	return JSON.parse($.ajax({
		url: "/CarritoDelete",
		type: 'post',
		dataType: 'json',
		data:{
			param: valor
		},
		async: false,
		success: function(data){
			return data;
		}
	}).responseText);
}

function CarroGuardado(){	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	})
	return JSON.parse($.ajax({
		url: "/carritoguardado",
		type: 'post',
		dataType: 'json',
		async: false,
		success: function(data){
			return data;
		}
	}).responseText);
}

function ConfirmarCompra(array, id){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	})
	return JSON.parse($.ajax({
		url: "/confirmar-compra",
		type: 'post',
		dataType: 'json',
		data:{
			param: array,
			param2: id
		},
		async: false,
		success: function(data){
			return data;
		}
	}).responseText);
}

function filtro(){
	console.log($('#id-cat').val());
	console.log($('#cod_postal').val());
	console.log($('#barrio').val());
}
