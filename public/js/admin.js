$(function(){
	$('#sub-tienda').hide();
	$('#sub-productos').hide();

	$('#tienda').click(function(event) {
		$('#sub-tienda').toggle();
	});

	$('#productos').click(function(event) {
		$('#sub-productos').toggle();
	});


	// Formulario modificar perfil
	var barrio = $('#b-fixed').val();
	$('#Tnd-Mod-b').change(function(event) {
		barrio = $('#Tnd-Mod-b option:selected').val();
	});

	var postal = $('#p-fixed').val();
	$('#Tnd-Mod-cd').change(function(event) {
		postal = $('#Tnd-Mod-cd option:selected').val();
	});

	$('#Tnd-Mod-form').submit(function(event) {
		var correcto = true;

		//nombre
		if($('#Tnd-Mod-n').val().length == 0){
			$('#Tnd-error-n').show();
			$('#Tnd-error-n').text('Este campo no puede estar vacio');	
			correcto = false;
		}else{
			$('#Tnd-error-n').hide();
		}
		//cif
		if($('#Tnd-Mod-c').val().length == 0){
			$('#Tnd-error-c').show();
			$('#Tnd-error-c').text('Este campo no puede estar vacio');	
			correcto = false;
		}else{
			$('#Tnd-error-c').hide();
		}
		//direccion
		if($('#Tnd-Mod-d').val().length == 0){
			$('#Tnd-error-d').show();
			$('#Tnd-error-d').text('Este campo no puede estar vacio');	
			correcto = false;
		}else{
			$('#Tnd-error-d').hide();
		}
		//telefono
		if($('#Tnd-Mod-t').val().length == 0){
			$('#Tnd-error-t').show();
			$('#Tnd-error-t').text('Este campo no puede estar vacio');	
			correcto = false;
		}else{
			$('#Tnd-error-t').hide();
		}
		
		if(correcto){
			return true;
		}else{
			return false;
		}
	});

	//formulario para añadir productos
	$('#Form-add').submit(function(event) {

		var correcto = true;

		var foto = $('#add-imagen').val();


		var imagen = foto.substr(-3, foto.indexOf('.'));

		if(imagen != 'jpg' && imagen != 'png' && imagen !='' ){
			correcto = false;
			$('#Er-add-imagen').show();
			$('#Er-add-imagen').text('Este archivo no es una imagen');
		}else{
			$('#Er-add-imagen').hide();
		}

		if($('#add-nombre').val().length == 0){
			correcto = false;
			$('#Er-add-nombre').show();
			$('#Er-add-nombre').text('Este campo no puede estar vacio');
		}else{
			$('#Er-add-nombre').hide();
		}

		if($('#add-stock').val().length == 0){
			correcto = false;
			$('#Er-add-stock').show();
			$('#Er-add-stock').text('Este campo no puede estar vacio');
		}else{
			$('#Er-add-stock').hide();
		}


		if($('#add-precio').val().length == 0){
			correcto = false;
			$('#Er-add-precio').show();
			$('#Er-add-precio').text('Este campo no puede estar vacio');
		}else{
			$('#Er-add-precio').hide();
		}

		if (correcto) {
			return true;
		}else{
			return false;
		}
	});

})