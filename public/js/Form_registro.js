

$(function(){

	$('#Reg-form').submit(function(event) {
		return false;
	});

	$('#Reg-user').focusout(function(event) {
		ExisteUsuario($(this).val());
	});

	$('#Reg-email').focusout(function(event) {
		ExisteEmail($(this).val());
	});
	
	$('#btn-reg-conf').click(function() {

		var bool = true;
		
		var regu = $('#Reg-user').val();
		if (regu.length < 5) {
			bool = false;
			$('#Error-user').show();
			$('#Error-user').text('El usuario debe tener un minimo de 5 digitos');
		}else{
			$('#Error-user').hide();
		}

		var rege = $('#Reg-email').val();
		if (rege.length < 5) {
			bool = false;
			$('#Error-email').show();
			$('#Error-email').text('Este campo es obligatorio de rellenar');
		}else{
			$('#Error-email').hide();
		}		

		var regp = $('#Reg-password').val();
		if (regp.length < 8) {
			bool = false;
			$('#Error-pass').show();
			$('#Error-pass').text('La contraseña debe tener un minimo de 8 digitos');
		}else{
			$('#Error-pass').hide();
		}

		var regn = $('#Reg-nombre').val();
		if (regn.length == 0) {
			bool = false;
			$('#Error-nombre').show();
			$('#Error-nombre').text('Este campo es obligatorio de rellenar');
		}else if(regn.length < 4){
			bool = false;
			$('#Error-nombre').show();
			$('#Error-nombre').text('El nombre debe tener un minimo de 4 letras');
		}else{
			$('#Error-nombre').hide();
		}

		var rega = $('#Reg-apellidos').val();
		if (rega.length <= 0) {
			bool = false;
			$('#Error-cognom').show();
			$('#Error-cognom').text('Este campo es obligatorio de rellenar');
		}else if(rega.length < 4){
			bool= false;
			$('#Error-cognom').show();
			$('#Error-cognom').text('El apellido debe tener un minimo de 4 letras');
		}else{
			$('#Error-cognom').hide();
		}

		var regt = $('#Reg-tel').val();
		if (regt.length == 0) {
			bool = false;
			$('#Error-tel').show();
			$('#Error-tel').text('Este campo es obligatorio de rellenar');
		}else{
			$('#Error-tel').hide();
		}

		var regd = $('#Reg-date').val();
		if (regd.length == 0) {
			bool = false;
			$('#Error-date').show();
			$('#Error-date').text('Este campo es obligatorio de rellenar');
		}else{
			$('#Error-date').hide();
		}

		var regc = $('#Reg-cod').val();
		if (regc.length == 0) {
			bool = false;
			$('#Error-cod').show();
			$('#Error-cod').text('Este campo es obligatorio de rellenar');
		}else{
			$('#Error-cod').hide();
		}

		var regr = $('#Reg-direccion').val();
		if (regr.length == 0) {
			bool = false;
			$('#Error-direccion').show();
			$('#Error-direccion').text('Este campo es obligatorio de rellenar');
		}else{
			$('#Error-direccion').hide();
		}

		if(bool){
			InsertarUsuario(regu, rege, regp, regn, rega, regt, regd, regc, regr);
		}
	});
});

function ExisteUsuario(valor){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	})
	$.ajax({
		url: "/registrar/comprobarusuario/" + $('#Reg-user').val(),
		type: 'post',
		dataType: 'json',
	})
	.done(function(data){
		if ( data.User === true ) {
			$('#btn-reg-conf').prop('disabled', false);
			$('#Error-user').hide();
		}else{
			$('#btn-reg-conf').prop('disabled', true);
			$('#Error-user').show();
			$('#Error-user').text('Error este usuario ya existe');
		}
	});
}

function ExisteEmail(valor){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	})
	$.ajax({
		url: "/registrar/comprobaremail/" + valor,
		type: 'post',
		dataType: 'json',
	})
	.done(function(data){
		if ( data.Email === true ) {
			$('#btn-reg-conf').prop('disabled', false);
			$('#Error-email').hide();
		}else{
			$('#btn-reg-conf').prop('disabled', true);
			$('#Error-email').show();
			$('#Error-email').text('Error este email ya existe');
		}
	});
}

function InsertarUsuario(u, e, p, n, a, t, d, c, r){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	})
	$.ajax({
		url: "/resgistrado=ok",
		type: 'post',
		dataType: 'json',
		data: {
			param1: u,
			param2: e,
			param3: p,
			param4: n,
			param5: a,
			param6: t,
			param7: d,
			param8: c,
			param9: r
		},
	})
	.done(function(data){
		var correcto = true;
		if(data.usuario == false){
			$('#Error-user').show();
			$('#Error-user').text('Error este usuario ya existe');
			correcto = false;
		}
		if(data.email == false){
			$('#Error-email').show();
			$('#Error-email').text('Error este email ya existe');
			correcto = false;
		}
		if (correcto) {
			var options =  {
				content: "Se ha registrado correctamente", 
				style: "snackbar",
				timeout: 1500
			}
			$.snackbar(options);

			setTimeout("redireccionarPagina()", 1800);
		};
	});	
}

function redireccionarPagina() {
	window.location = "http://baldu.com";
}