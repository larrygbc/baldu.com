<?php

namespace App\Http\Middleware;

use Closure;
use Session;


class AuthUser
{
    
    public function handle($request, Closure $next)
    {
        if (empty(Session::get('usuario'))) {
            return redirect('/');        
        }
        return $next($request);
    }
}
