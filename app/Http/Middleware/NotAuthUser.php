<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class NotAuthUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!empty(Session::get('usuario'))) {
            return redirect("/auth/" . Session::get('usuario'));        
        }
        return $next($request);
    }
}
