<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Storage;



class TiendaController extends Controller
{
    public function IniciarSesion($mensaje = " "){
        // Vista login tienda
        return view('Tienda/login', ['mensaje' => $mensaje]);
    }

    public function RegistrarTienda(){
        // Vista del formulario de registro
        return view('Tienda/RegistrarTienda');
    }

    //Vista index de la tienda
    public function Login($id){
        return view('Tienda/index',[
            'nombre' => Session::get('nombre'),
            'email' => Session::get('email'),
            'id' => Session::get('id'),
            'direccion' => Session::get('direccion'),
            'codigo_postal' => Session::get('codigo_postal'),
            'cif' => Session::get('cif'),
            'barrio' => Session::get('barrio'),
            'telefono' => Session::get('telefono'),
        ]);
    }

    // Vista Perfil de la tienda
    public function Perfil($id){
        return view('Tienda/perfil',[
            'nombre' => Session::get('nombre'),
            'email' => Session::get('email'),
            'id' => Session::get('id'),
            'direccion' => Session::get('direccion'),
            'codigo_postal' => Session::get('codigo_postal'),
            'cif' => Session::get('cif'),
            'barrio' => Session::get('barrio'),
            'telefono' => Session::get('telefono'),
        ]);
    }

    // Vista modificar tienda
    public function ModificarPerfil($id){

        $c_postal = \DB::table('codigo_postal')->
        get();

        $barrio = \DB::table('barrio')->
        get();


        return view('Tienda/modificar_perfil',[
            'nombre' => Session::get('nombre'),
            'email' => Session::get('email'),
            'id' => Session::get('id'),
            'direccion' => Session::get('direccion'),
            'codigo_postal' => $c_postal,
            'sel_cod' => Session::get('codigo_postal'),
            'cif' => Session::get('cif'),
            'sel_barrio' => Session::get('barrio'),
            'barrio' => $barrio,
            'telefono' => Session::get('telefono')
        ]);
    }
    
    //confirmacion al modificar cuenta de una tienda
    public function ModificarAdmin(Request $request, $id){
        $postal = \DB::table('codigo_postal')->
        where('codigo_postal', $request->Tnd_Mod_cod)->
        value('id');

        $barri = \DB::table('barrio')->
        where('nombre', $request->Tnd_Mod_barrio)->
        value('id');

        $update = \DB::table('tienda')
        ->where('id', $id)
        ->update([
            'cif' => $request->Tnd_Mod_cif,
            'nombre' => $request->Tnd_Mod_nombre,
            'codigo_postal' => $postal,
            'direccion' => $request->Tnd_Mod_direccion,
            'barrio' => $barri,
            'telefono' => $request->Tnd_Mod_tel
        ]);

        $c_postal = \DB::table('codigo_postal')->
        where('id', $postal)->
        value('codigo_postal');

        $barrio = \DB::table('barrio')->
        where('id', $barri)->
        value('nombre');


        Session::put('nombre', $request->Tnd_Mod_nombre);
        Session::put('cif', $request->Tnd_Mod_cif);
        Session::put('direccion', $request->Tnd_Mod_direccion);
        Session::put('codigo_postal', $c_postal);
        Session::put('barrio', $barrio);
        Session::put('telefono', $request->Tnd_Mod_tel);

        return redirect()->action('TiendaController@ModificarPerfil', $id);
    }

    //Vista para añadir un producto
    public function AñadirProducto($id){
        $consulta = \DB::table('categoria')->
        get();


        return view('Tienda/añadir_producto',[
            'id' => Session::get('id'),
            'categoria' => $consulta
        ]);
    }

    //confrimacion al añadir un producto
    public function GuardarProducto(Request $request, $id){
        $ruta="";

        if($request->file('imagen')){
            $ruta = Storage::disk('public')->put("Tienda/$id/productos", $request->file('imagen'));
        }
        
        $consulta = \DB::table('categoria')->
        where('categoria', $request->categoria)->
        value('id');

        $insertar = \DB::table('producto')->
        insert([
            'nombre' => $request->nombre,
            'descripcion' => $request->descripcion,
            'stock' => $request->stock,
            'precio' => $request->precio,
            'categoria' => $consulta,
            'ruta' => $ruta
        ]);

        $producto = \DB::table('producto')->
        where('ruta', $ruta)->
        value('id');


        $insertEsta = \DB::table('esta')->
        insert([
            'id_producto' => $producto,
            'id_tienda' => $id
        ]);


        return redirect()->action('TiendaController@AñadirProducto', $id);
    }

    //Vista de modificar un producto
    public function ModificarProducto(Request $request, $id){
        $productos = \DB::table('producto')->
        join('esta', 'esta.id_producto', '=', 'producto.id')->
        where('esta.id_tienda', $id)->
        get();

        return view('Tienda/modificar_producto', [
            'id' => $id,
            'productos' => $productos
        ]);
    }

    // Vista de los productos de una tienda
    public function Producto($id, $id_producto){

        $producto = \DB::table('producto')->
        where('id', $id_producto)->
        get();

        $categoria = \DB::table('categoria')->
        get();

        return view('Tienda/producto', [
            'id' => $id,
            'categoria' => $categoria,
            'producto' => $producto
        ]);
    }

    //Confirmacion de actualizar un producto
    public function ProductoOk(Request $request, $id, $id_producto){

        $consulta = \DB::table('producto')->
        where('id', $id_producto)->
        value('ruta');

        if($request->file('imagen')==null){

            $update = \DB::table('producto')->
            where('id', $id_producto)->        
            update([
                'nombre' => $request->nombre,
                'stock' => $request->stock,
                'precio' => $request->precio,
                'categoria' => $request->categoria,
            ]);

        }else{
            Storage::disk('public')->delete($consulta);
            
            $ruta = Storage::disk('public')->put("Tienda/$id/productos", $request->file('imagen'));

            $update = \DB::table('producto')->
            where('id', $id_producto)->        
            update([
                'nombre' => $request->nombre,
                'stock' => $request->stock,
                'precio' => $request->precio,
                'categoria' => $request->categoria,
                'ruta' => $ruta
            ]);
        }

        return redirect()->action('TiendaController@ModificarProducto', $id);
    }

    // Vista eleiminar producto
    public function EliminarProducto($id){
        $productos = \DB::table('producto')->
        join('esta', 'esta.id_producto', '=', 'producto.id')->
        where('esta.id_tienda', $id)->
        get();

        return view('Tienda/eliminar_producto', [
            'id' => $id,
            'productos' => $productos
        ]);
    }

    //Eliminar un producto
    public function Eliminar($id, $id_producto){
        $consulta = \DB::table('producto')->
        where('id', $id_producto)->
        value('ruta');

        Storage::disk('public')->delete($consulta);

        \DB::table('esta')->where('id_producto', $id_producto)->delete();

        \DB::table('producto')->where('id', $id_producto)->delete();

        return redirect()->action('TiendaController@EliminarProducto', $id);   
    }

    //cerrar sesion
    public function CerrarSesion(){
        Session::flush();

        return redirect()->action('HomeController@getHome');

    }

    // Vista de recuperar-contraseña
    public function RecuperarContraseña($mensaje = " "){
        return view('Tienda/Recuperar_contraseña', ['mensaje' => $mensaje]);
    }

    //Borrar cuenta
    public function BorrarCuenta($id){
        
        Session::flush();

        $tienda = \DB::table('esta')->
        join('producto', 'esta.id_producto', '=', 'producto.id')->
        join('tienda', 'esta.id_tienda', '=', 'tienda.id')->
        where('tienda.id', $id)->
        select('producto.id')->
        get();

        for ($i = 0; $i < count($tienda); $i++) {
            \DB::table('producto')->where('id', $tienda[$i]->id)->delete();
        }
        
        \DB::table('tienda')->where('id', $id)->delete();

        return redirect()->action('HomeController@getHome');
    }

    //ajax
    public function RegistroCodigoPostal(){
        $consulta = \DB::table('codigo_postal')->
        get();
        return response()->json($consulta);
    }

    public function RegistroBarrio(){
        $consulta = \DB::table('barrio')->
        get();
        return response()->json($consulta);
    }
    public function ValidarTiendaEmail(Request $request){

        $consulta = \DB::table('tienda')->
        where('email', $request->param1)->
        get();

        $existe = false;
        if(count($consulta) > 0){
            $existe = true;
        }

        if($existe){
            return response()->json(true);
        }else{
            return response()->json(false);
        }
    }
}
