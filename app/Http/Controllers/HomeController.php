<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public $carrito = array();

    //vista index sin usuario autenticado
    public function getHome(){
        $consulta = \DB::table('producto')->
        get();

        $data= [];

        foreach ($consulta as $value) {
            array_push($data, $value);
        }
        shuffle($data);

        return view('index', array('productos'=> $data) );
    }
    // vista para iniciar sesion
    public function getIniciarSesion($mensaje = " "){
    	return view('IniciarSesion', ['mensaje' => $mensaje]);
    }
    //vistas para registrar usuario
    public function getRegistrar(){
        return view('RegistrarUsuario');
    }
    //pagina de inicio una vez inicio sesion
    public function getLogin($id){

        $consulta = \DB::table('producto')->
        get();

        $usuario = \DB::table('usuario')->
        where('usuario', $id)->
        get();

        $data= [];

        foreach ($consulta as $value) {
            array_push($data, $value);
        }

        shuffle($data);

        return view('login', array(
            'S_usuario' => Session::get('usuario'),
            'S_email' => Session::get('email'),
            'S_nombre' => Session::get('nombre'),
            'productos' => $data,
            'usuario' => $usuario
        ));
    }
    //cerrar sesion
    public function getLogout(){
    	Session::flush();
    	return redirect()->action('HomeController@getHome');
    }
    //busqueda de producto
    public function getbusqueda(Request $request){

        $valor = $request->search;

        $usuario = \DB::table('usuario')->
        where('usuario', Session::get('usuario'))->
        get();

        //condicion de busqueda con filtro o sin filtro
        if(isset($request->filtro)){

            //condicion cuando categoria equivale a todas las categorias o no
            if ($request->sel_cat == 1) {
                
                $barrio = $request->sel_barrio;
                $codigo_postal = $request->sel_cod;

                $consulta = \DB::table('esta')->
                join('producto', 'esta.id_producto', '=', 'producto.id')->
                join('tienda', 'esta.id_tienda', '=', 'tienda.id')->
                where('producto.nombre', 'like', '%' . $valor . '%')->
                whereBetween('precio', [$request->sel_min, $request->sel_max])->
                where(function ($query) use ($codigo_postal, $barrio){
                    $query->where('tienda.barrio', '=', $barrio)->
                            orWhere('tienda.codigo_postal', '=', $codigo_postal);
                })->
                select('tienda.codigo_postal', 'tienda.barrio', 'producto.*')->
                get();
            }else{
                $consulta = \DB::table('esta')->
                join('producto', 'esta.id_producto', '=', 'producto.id')->
                join('tienda', 'esta.id_tienda', '=', 'tienda.id')->
                where('producto.nombre', 'like', '%' . $valor . '%')->
                where('categoria', $request->sel_cat)->
                where('tienda.barrio', $request->sel_barrio)->
                orWhere('tienda.codigo_postal', $request->sel_cod)->
                whereBetween('precio', [$request->sel_min, $request->sel_max])->
                where(function ($query) use ($codigo_postal, $barrio){
                    $query->where('tienda.barrio', '=', $barrio)->
                            orWhere('tienda.codigo_postal', '=', $codigo_postal);
                })->
                select('tienda.codigo_postal', 'tienda.barrio', 'producto.*')->
                get();
            }

            $data= [];

            foreach ($consulta as $value) {
                array_push($data, $value);
            }

            shuffle($data);

            return view('login', array(
                'S_usuario' => Session::get('usuario'),
                'S_email' => Session::get('email'),
                'S_nombre' => Session::get('nombre'),
                'productos' => $data,
                'usuario' => $usuario
            ));

        }else{
            //busqueda simple
            $consulta = \DB::table('producto')->
            where('nombre', 'like', '%' . $valor . '%')->
            get();

            $data= [];

            foreach ($consulta as $value) {
                array_push($data, $value);
            }

            shuffle($data);

            return view('login', array(
                'S_usuario' => Session::get('usuario'),
                'S_email' => Session::get('email'),
                'S_nombre' => Session::get('nombre'),
                'productos' => $data,
                'usuario' => $usuario
            ));
        }  
    }

     //busqueda de producto sin iniciar sesion
    public function getbusquedaIndex(Request $request){

        $valor = $request->search;

        //condicion de busqueda con filtro o sin filtro
        if(isset($request->filtro)){

            //condicion cuando categoria equivale a todas las categorias o no
            if ($request->sel_cat == 1) {
                
                $barrio = $request->sel_barrio;
                $codigo_postal = $request->sel_cod;

                $consulta = \DB::table('esta')->
                join('producto', 'esta.id_producto', '=', 'producto.id')->
                join('tienda', 'esta.id_tienda', '=', 'tienda.id')->
                where('producto.nombre', 'like', '%' . $valor . '%')->
                whereBetween('precio', [$request->sel_min, $request->sel_max])->
                where(function ($query) use ($codigo_postal, $barrio){
                    $query->where('tienda.barrio', '=', $barrio)->
                            orWhere('tienda.codigo_postal', '=', $codigo_postal);
                })->
                select('tienda.codigo_postal', 'tienda.barrio', 'producto.*')->
                get();
            }else{
                $consulta = \DB::table('esta')->
                join('producto', 'esta.id_producto', '=', 'producto.id')->
                join('tienda', 'esta.id_tienda', '=', 'tienda.id')->
                where('producto.nombre', 'like', '%' . $valor . '%')->
                where('categoria', $request->sel_cat)->
                where('tienda.barrio', $request->sel_barrio)->
                orWhere('tienda.codigo_postal', $request->sel_cod)->
                whereBetween('precio', [$request->sel_min, $request->sel_max])->
                where(function ($query) use ($codigo_postal, $barrio){
                    $query->where('tienda.barrio', '=', $barrio)->
                            orWhere('tienda.codigo_postal', '=', $codigo_postal);
                })->
                select('tienda.codigo_postal', 'tienda.barrio', 'producto.*')->
                get();
            }

            $data= [];

            foreach ($consulta as $value) {
                array_push($data, $value);
            }

            shuffle($data);

            return view('index', array(
                'productos' => $data
            ));

        }else{
            //busqueda simple
            $consulta = \DB::table('producto')->
            where('nombre', 'like', '%' . $valor . '%')->
            get();

            $data= [];

            foreach ($consulta as $value) {
                array_push($data, $value);
            }

            shuffle($data);

            return view('index', array(
                'productos' => $data
            ));
        }        
    }

    //vista del perfil de usuario
    public function Perfil($id){

        $consulta = \DB::table('usuario')->
        where('usuario', $id)->
        get();

        $codigo_postal = \DB::table('codigo_postal')
        ->get();

        return view('perfil', [
            'S_usuario' => Session::get('usuario'),
            'S_email' => Session::get('email'),
            'S_nombre' => Session::get('nombre'),
            'codigo_postal' => $codigo_postal,
            'usuario' => $consulta
        ]);
    }

    //modificar datos del usuario
    public function ModificarPerfil(Request $request, $id){

        $consulta = \DB::table('usuario')->
        where('id', $id)->
        value('ruta');

        if($request->file('imagen') == null){

            \DB::table('usuario')->
            where('usuario', $id)->
            update([
                'nombre' => $request->pfl_nombre,
                'apellidos' => $request->pfl_apellidos,
                'telefono' => $request->pfl_tel,
                'fecha_nacimiento' => $request->pfl_date,
                'codigo_postal' => $request->pfl_cod,
                'direccion' => $request->pfl_direccion
            ]);

        }else{
            Storage::disk('public')->delete($consulta);

            $ruta = Storage::disk('public')->put("Usuario/$id", $request->file('imagen'));

            \DB::table('usuario')->
            where('usuario', $id)->
            update([
                'nombre' => $request->pfl_nombre,
                'apellidos' => $request->pfl_apellidos,
                'telefono' => $request->pfl_tel,
                'fecha_nacimiento' => $request->pfl_date,
                'codigo_postal' => $request->pfl_cod,
                'direccion' => $request->pfl_direccion,
                'ruta' => $ruta
            ]);
        }

        return redirect()->action('HomeController@Perfil', $id);
    }

    //vista del proceso de compra
    public function getConfirmarCompra($id){
        $consulta = \DB::table('usuario')
        ->where('usuario', $id)
        ->get();


        return view('Confirmar_compra', ([
            'S_usuario' => Session::get('usuario'),
            'S_email' => Session::get('email'),
            'S_nombre' => Session::get('nombre'),
            'productos' => Session::get('carro'),
            'usuario' => $consulta
        ]));
    }
    // vista de las compras del usuario
    public function Compras($id){
        $usuario = \DB::table('usuario')->
        where('usuario', $id)-> 
        get();

        $id_usuario = "";
        foreach ($usuario as $value) {
            $id_usuario = $value->id;
        }

        $consulta = \DB::table('compra')->
        where('id_usuario', $id_usuario)->
        get();

        return view('compras', [
            'S_usuario' => Session::get('usuario'),
            'S_email' => Session::get('email'),
            'S_nombre' => Session::get('nombre'),
            'productos' => Session::get('carro'),
            'compras' => $consulta,
            'usuario' => $usuario
        ]);

    }
    //vista de los detalles de una compra especifica
    public function DetalleCompra($id, $id_compra){
        $usuario = \DB::table('usuario')->
        where('usuario', $id)-> 
        get();

        $productos = \DB::table('contiene')->
        join('producto', 'producto.id', '=', 'contiene.id_producto')->
        where('contiene.id_compra', $id_compra)->
        get();

        return view('Detalle_compra', [
            'S_usuario' => Session::get('usuario'),
            'S_email' => Session::get('email'),
            'S_nombre' => Session::get('nombre'),
            'productos' => Session::get('carro'),
            'usuario' => $usuario,
            'productos' => $productos
        ]);
    }
    //vista para restablecer contraseña
    public function RecuperarContraseña($mensaje = " "){
        return view('Recuperar_contraseña', ['mensaje' => $mensaje]);
    }

    //Borrar cuenta
    public function BorrarCuenta($id){
        Session::flush();

        \DB::table('usuario')->where('usuario', $id)->delete();

        return redirect()->action('HomeController@getHome');
    }


    //consulta ajax

    //añadir productos a un carrito de compra
    public function postCarrito(Request $request){
        //array
        Session::push('array', $request->param);

        $array = Session::get('array');

        $data = [];
        foreach ($array as $key => $value) {
            $consulta = \DB::table('producto')->
            where('id',  $array[$key])->
            get();

            foreach ($consulta as $value) {
                array_push($data, $value);
            }
        }

        Session::put('carro', $data);

        return response()->json($data);
    }
    //enviar carrito de compra
    public function postCarritoGuardado(){
        return response()->json(Session::get('carro'));
    }
    //borrar productos de un carrito de compra
    public function postCarritoEliminar(Request $request){

        $array = Session::get('array');

        $clave = array_search("$request->param", $array);

        unset($array[$clave]);

        Session::put('array', $array);

        $array = Session::get('array');

        $data = [];
        foreach ($array as $key => $value) {
            $consulta = \DB::table('producto')->
            where('id',  $array[$key])->
            get();

            foreach ($consulta as $value) {
                array_push($data, $value);
            }
        }

        Session::put('carro', $data);

        return response()->json($data);
    }

    //hacer efectiva la compra
    public function ValidarCompra(Request $request){
        $array = $request->param;

        $id = str_shuffle("baldu".uniqid());

        $total = 0;
        for ($i = 0; $i < count($array); $i++) {
            $total = $total + $array[$i]['precio'];
        }

        $fecha_actual = getdate();

        $dia = $fecha_actual['mday'];
        $año = $fecha_actual['year'];
        $mes = $fecha_actual['mon'];

        $fecha = "$año-$mes-$dia"; 

        $consulta = \DB::table('usuario')->
        where('usuario', $request->param2)->
        value('id');

        \DB::table('compra')->insert([
            'id' => $id,
            'fecha_compra' => $fecha,
            'importe_total' => $total,
            'id_usuario' => $consulta
        ]);

        for ($i = 0; $i < count($array); $i++) {
            \DB::table('contiene')->insert([
                'id_compra' => $id,
                'id_producto' => $array[$i]['id']
            ]);
        }

        Session::forget('array');
        Session::forget('carro');

        return response()->json(true);
    }

    //añadir informacion al filtro
    public function postfiltro(){
        $codigo_postal = \DB::table('codigo_postal')->get();

        $barrios = \DB::table('barrio')->get();

        $categorias = \DB::table('categoria')->get();

        return response()->json(['barrio' => $barrios, 'codigo_postal' => $codigo_postal, 'categorias' => $categorias]);
    }

}