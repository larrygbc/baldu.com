<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Http\Controllers\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderShipped;

class ValidarController extends Controller
{

    //inicio de sesion del lado tienda
    public function ValidarTienda(Request $request){
        $pass = $request->password;
        $email = $request->email;

        $consulta = \DB::table('tienda')->
        where('email', $email)->
        get();

        foreach ($consulta as $value) {
            $id = $value->id;
            $correo = $value->email;
            $nom = $value->nombre;
            $direccion = $value->direccion;
            $cod = $value->codigo_postal;
            $password = $value->password;
            $barrio = $value->barrio;
            $cif = $value->cif;
            $tel = $value->telefono;
        }

        
        
        if (count($consulta) > 0 ) {

            $c_postal = \DB::table('codigo_postal')->
            where('id', $cod)->
            value('codigo_postal');

            $barri = \DB::table('barrio')->
            where('id', $barrio)->
            value('nombre');

            if (Hash::check($pass, $password)) {
                Session::put('nombre', $nom);
                Session::put('cif', $cif);
                Session::put('email', $correo);
                Session::put('id', $id);
                Session::put('direccion', $direccion);
                Session::put('codigo_postal', $c_postal);
                Session::put('password', $password);
                Session::put('barrio', $barri);
                Session::put('telefono', $tel);
                return redirect()->action('TiendaController@Login', $id);
            }else{
                return redirect()->action('TiendaController@IniciarSesion', "La contraseña es incorrecta");
            }
        }else{
            return redirect()->action('TiendaController@IniciarSesion', "Esta tienda no esta registrada en baldu");
        }
    }

    // restaurar contraseña del lado cliente
    public function Contraseña(Request $request){

        $consulta = \DB::table('usuario')->
        where('email', $request->email)->
        get();

        if(count($consulta) > 0){
            $update = \DB::table('usuario')->
            where('email', $request->email)->
            update([
                'password' => Hash::make($request->Password1)
            ]);

            Mail::to($request->email)->send(new OrderShipped($request));

            return redirect()->action('HomeController@getIniciarSesion');

        }else{
            return redirect()->action('HomeController@RecuperarContraseña', ['mensaje' => 'Error, este email no esta registrado en baldu']);
        }

    }

    // restaurar contraseña del lado tienda
    public function ContraseñaTienda(Request $request){

        $consulta = \DB::table('tienda')->
        where('email', $request->email)->
        get();

        if(count($consulta) > 0){
            $update = \DB::table('usuario')->
            where('email', $request->email)->
            update([
                'password' => Hash::make($request->Password1)
            ]);

            Mail::to($request->email)->send(new OrderShipped($request));

            return redirect()->action('TiendaController@IniciarSesion');

        }else{
            return redirect()->action('TiendaController@RecuperarContraseña', ['mensaje' => 'Error, este email no esta registrado en baldu']);
        }

    }

    //ajax
    //comprobar que existe el email
    public function postComprobarEmail($id){
        $consulta = \DB::table('usuario')->
        where('email', $id)->
        value('email');

        if(count($consulta) > 0 ){
            return response()->json(['Email' => false]);
        }else{
            return response()->json(['Email' => true]);
        }
    }

    // registrar tienda
    public function RegistroTienda(Request $request){
        $consulta= \DB::table('tienda')->
        where('email', $request->param2)->
        get();

        $existe = true;
        if(count($consulta) > 0 ){
            $existe = false;
        }

        if($existe){
            $id = str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789".uniqid());

            \DB::table('tienda')->insert([
                'id' => $id,
                'cif' => $request->param4,
                'email' => $request->param2,
                'nombre' => $request->param1,
                'codigo_postal' => $request->param5,
                'barrio' => $request->param6,
                'direccion' => $request->param7,
                'telefono' => $request->param8,
                'valoracion' => '3',
                'password' => Hash::make($request->param3)
            ]);

            return response()->json(true);
        }else {
            return response()->json(false);
        }    
    }

     //resgistrar cliente
    public function postRegistrar(Request $request){
        $usuario = true;
        $email = true;

        $ConsultaUser = \DB::table('usuario')->
        where('usuario', $request->param1)->get();

        if(count($ConsultaUser) > 0){
            $usuario = false;
        }

        $ConsultaEmail = \DB::table('usuario')->
        where('email', $request->param2)->get();

        if(count($ConsultaEmail) > 0){
            $email = false;
        }

        if ($usuario && $email){
            \DB::table('usuario')->insert([
                'usuario' => $request->param1,
                'nombre' => $request->param4,
                'apellidos' => $request->param5,
                'telefono' => $request->param6,
                'fecha_nacimiento' => $request->param7,
                'codigo_postal' => $request->param8,
                'email' => $request->param2,
                'password' => Hash::make($request->param3),
                'direccion' => $request->param9
            ]);
        }
        
        return response()->json(['usuario' => $usuario, 'email' => $email]);
    }

    //inicio de sesion del cliente
    public function postValidarUsuario(Request $request){
        $pass = $request->UsuPassword;
        $id = $request->UsuId;

        $consulta = \DB::table('usuario')->
        where('usuario', $id)->
        orwhere('email', $id)->
        get();

        foreach ($consulta as $value) {
            $nombre = $value->nombre;
            $apellidos = $value->apellidos;
            $usuario = $value->usuario;
            $password = $value->password;
            $email = $value->email;
        }
        

        if (count($consulta) > 0 ) {
            $nombre_completo = $nombre . " " . $apellidos;

            if (Hash::check($pass, $password)) {
                Session::put('nombre', $nombre_completo);
                Session::put('email', $email);
                Session::put('usuario', $usuario);
                Session::put('array', array());
                return redirect()->action('HomeController@getLogin', $usuario);
            }else{
                return redirect()->action('HomeController@getIniciarSesion' , "La contraseña es incorrecta");
            }
        }else{
            return redirect()->action('HomeController@getIniciarSesion' , "Este usuario no esta registrado en baldu");
        }
    }

    //coprobar si existe un usuario
    public function postComprobarUser($id){
        $consulta = \DB::table('usuario')->
        where('usuario', $id)->
        value('usuario');

        if(count($consulta) > 0 ){
            return response()->json(['User' => false]);
        }else{
            return response()->json(['User' => true]);
        }        
    }

}
